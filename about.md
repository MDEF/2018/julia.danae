---
layout: page
title:
permalink: /about/
---

![]({{site.baseurl}}/images/AboutJuliaImage.jpg)
*Photograph by Sourwhat Yun, Belgium, 2017*

Brought up in the countryside in Belgium, I was introduced early on to the natural environment, and
developed a strong understanding of the importance of having the living world as part of our
human engineered world.
In the past four years, I was trained to be an interior spatial designer in the University of the Arts
London: Chelsea College of Arts, and an interior architect and furniture designer in
Konstfack: University of the Arts, Crafts and Design in Stockholm.
These experiences have allowed me to develop meaningful concepts and more recently to create
speculations focusing on today’s big challenges.
I take a multidisciplinary approach in my work, constantly challenging the boundaries between
design, art, and science; I design spaces, atmospheres, structures, experiences with the aim to
design more sustainable futures.
