---
title: Design for the New
period: 25 April 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Markel Cormenzana and Mercè Rua from HOLON*</span>

## Design for the New

### Learning process and integration into project

**Practice theory**

​	As Reckwitz describes it in his diagram, practice theory is issued from cultural theory as part of social theory. It is defined as a ‘routinized type of behaviour’ consisting in various elements which are interconnected to one another, in the form of bodily, mental activities and objects and their use but also ‘ a background knowledge in the form of understanding’, ‘state of emotion and motivational knowledge.’ (Reckwitz).

We’ve been introduced to the images, skills and stuff model adapted from Shove and Pantzar (2005) as a way to deconstruct and explore practices.

| **Stuff**                                                    | **Skills**                                                   | **Images**                                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Tangible, material elements   (objects, infrastructures, tools, hardware and body) | Learned bodily and mental routines. Learn by doing. (ways of feeling and doing, know-how) | Socially shared ideas or concepts. (to give meaning, engage or reason a practice) |

To experience the relevance of specific social practice related to my project, I have mapped the areas of interest, the actor-network (actants) and social practices.

This mapping tried to answer the following questions:

- Which actants affect or are affected by my project?
- Which are their needs and demands?
- How do actants relate?
- Which social practices can you identify from the map we have built?
- Can you track the historical progress that this practice has suffered?

And finally I defined my area of intervention within the map created and tried to establish what are the main disruptions my intervention is suggesting.



**Practice map of communicating (by touching and capturing), version 0.1**

![]({{site.baseurl}}/Practice Map.png)

**Practice map of communicating (by touching and capturing), version 0.2**

![]({{site.baseurl}}/Practice Map 0.2.jpg)

**Current practice analysis**

With the digital world revealing itself evermore within the physical, the layers of abstractions of the digital are multiplying, increasing the inability for digital users to relate to its physical form in the process of communication. The constant flow of new data in the digital space is relying on information and knowledge exchange to keep redefining this space. To respond to this fluid nature of the digital, the physical tools supporting and framing this space are designed and produced with an increasing number of connections made between components. Those complex hidden connections result in a disconnection between the interactions and their implications. Humans are no longer able to relate to the digital and physical consequences of the growing digital world. As a result, there’s a disengagement in the physical world which causes a greater unawareness of the continuous depletion of abiotic resources, on which we greatly depend to sustain the current digital culture. The digital interfaces mediating this culture are reshaping our senses and ourselves, progressively leading to the loss of haptic.

My project is an exploration of the materials responsible for the interactions with two seperate world, yet dependent of one another. One as the abstract digital and the other as the tangible physical. It will reveal the physical manifestation of this digital manipulation, and try to redefine the conversation between the two. For this reason, I believe the practice of communicating is essential to my project.  

The exercise of describing the *stuff*, *skills* and the *images* now and in the future has greatly helped me understand the things my project is linking to and its possible improbable implications or influences in the more distant connections.

I have started by defining the elements of the practice in the present (and past that has been kept until today).

| Stuff                                                        | Skills                                                       | Images                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Analogue camera, digital camera, digital devices, artificial interfaces, objects of everyday life used to communicate, data, human body, natural environment, photographs, images, machines, light, energy, touchscreen, abiotic resources. | Seeing using a single touch, active communication, interactions, pressure, distant perception of surfaces and materials, short interactions, human machine relationship, representation, framing, capturing, sharing, hoarding, connecting virtually. | Identity defined not only by appearances but by mode of expression (movement, actions… Not the what but the how), capture ephemeral feeling, follow biological rhythm to communicate, listen to yourself (present example in dance and sports), video, pixels, emoji’s, artificial light, portrait, digital identity vs. physical reality. |

**Desired practice analysis**

The goal of my project being to engage and challenge the meaning of current interfaces and shape more responsible interactions in the use of such hybrid cultural objects, that is the smartphone today. The power of material identity and its political implications in the manipulation of it, have the potential to redefine the interface of the future. Which makes me question to what extent will interactions be physical or digital? What form will take the materials that mediate those conversations? What will redefine our interfaces? What will our interfaces be in the future? What will the physical manifestation evolve into?

I have now defined the elements of the practice in the future in order to make sure I will be creating a direction for the future that I wish for, considering in too the possible other implications.

| Stuff                                                        | Skills                                                       | Images                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Digital camera lens, digital devices, materiality of objects of everyday life used to communicate, data, human body, skin as a biological only interface, natural environment integrated into this connectivity, machines, time become tangible/material, biological internet of things, human body as the only biological element untransformed, protected by its hybrid environment. | Emotional attachment, seeing through touching, shaping with the hands, sensing with the entire body, responsible models of  communication, meaningful actions as part of  interactions, interactions, pressure, discovering surfaces and materials, human machine relationship, distribute, collective communication, adaptation, resilience, physical connection. | Identity defined not only by appearances but by mode of expression (movement, actions.Not the what but the how), capture ephemeral feeling, follow biological rhythm to communicate, listen to yourself (present example in dance and sports), personalisation, changing states of matter. |

**Intervention transition pathway**

*Latent Communication* aims to question how we give materiality to digital interactions, following the haptic sense. It envisions a process of communication redefined by the individual’s haptic exploration of a series of materials. In this sense, it is the material that will shape the process of digital and physical communication. The intervention will consist in revealing the beneficial possibilities of using touch to develop a sensorial literacy as a way to influence the choice of materials for the next generation digital interfaces.
