---
title: Term 2
period: 12 April 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

## 1| Initial project presentation

<object data="{{site.baseurl}}/resources/Latent Communication 19 February 2019.pdf" type="application/pdf" width="60%" height="720">
  <p>Alternative text - include a link <a href="{{site.baseurl}}/resources/Latent Communication 19 February 2019.pdf">to the PDF!</a></p>
</object>

### Project aim re-defined
*Explore the relationship between materials and human bodies to understand it as a endless process of becoming, necessary to redefine our hylomorphic model of society.*

### Statement
Explore the relationship between materials and human bodies to understand it as a endless process of becoming, necessary to redefine our hylomorphic model of society. The current division between nature and society, represented by the notion of materiality, needs to become one body in order to contribute to the wellbeing of individuals, within the urban context. This symbiosis can redefine our environment and ourselves towards a more responsible process.

### Introduction
Todays global technological advancements have developed a complex relationship between humans ant its environment.  The disparity between the way nature works and the way human think is blinding humanity to see the delicate inter-dependency between the two. Humans have adapted their environment unlike ecological systems, in a manner that is exhausting all essential resources.
Our ecological landscape is continuously evaporating from this semi-liquid model of society. Our fixed built environment is unable to follow the changing behaviour and respond to our emotional and physical needs. A society resistant to change, putting individuals in a state of confusion and fear, making us unable to relate to our environment.

A project positioned between nature, technology and human beings, this project attempts to converse in-between systems as a way to enable a change.

### Question
How could I develop an ecology of mind in the process of a material design, as a way to create a living symbiosis strengthening the relationship between living systems?

What is the nature of one’s relationship with a material and its environment? Can a living material make you more responsible - ability to respond - towards the natural environment? What information would I want to exchange with this material? Can an inorganic material replace an organic material to equally generate that proximity and responsibility? How can something that is not living give you the same benefits a living ecosystem can give? What hybrid process can enable a material with low embodied energy?

Looking at several inspirational project looking into those same lines, I question how do those materials behave in relation to their context?

### Objectives

- Hybrid process
- Ecology of mind
- Biomaterials
- Symbiosis.

Emotionally sensitive design, using emotions to trigger a change in the morphology of the object, developing an emotional and physical attachment over time as they co-evolve.
Energy efficient processes, using the potential of the dynamic nature of bacteria and other biological entities to reduce the embodied energy of an object.
Responsive material, adapting its behaviour to environmental, social and economic challenges found in ecological and urban environment.

- Living processes along with 3d and 2d printing, moulding, milling
- Haptic and emotional interaction in the process of making and co-evolving. A mirrored response ability.
- Materials defining an environment compatible with technology, digitalisation and ecological environment.
- Symbiotic process of change in a scaled up application.

### Digital interface
A tool.
Like most electronics, a screen is used to facilitate everyday life by providing access to information. It has no identity, easily replaceable and requires a short interaction time to fulfil the demand. However its production process demands and consumes a lot of energy. The embodied energy of such product is much more than we would imagine, requiring a lot more resources than just a finger touch.
What is a electronic device in an ecosystem of hybrid materials?
A touch screen is made of a capacitive surface using the human skin to conduct the energy to a electronic circuit, able to detect the location of the contact on the surface. What if the screen was using the design structure of the wing of a butterfly, to give a sensorial and emotional information through a haptic interaction? The human body The material The environment. Two biological systems conversing.

------------

## 2| Mid-term project update

<object data="{{site.baseurl}}/resources/LatentCommunicationMidCrit.pdf" type="application/pdf" width="60%" height="720">
  <p>Alternative text - include a link <a href="{{site.baseurl}}/resources/LatentCommunicationMidCrit.pdf">to the PDF!</a></p>
</object>

### Re-definition
>Explore the relationship between materials and human bodies to understand it as a endless process of becoming, necessary to redefine our hylomorphic model of society.


### Biophilia
- Edward O. Wilson
- Stephen R.Kellert

An innate tendency to focus on life and lifelike processes. Human health and well-being has a biologically-based need to affiliate with nature. Biophilic design as one that uses organic shapes and forms that directly, indirectly or symbolically elicit people’s inherent affinity for the natural environment.

### Biomimicry
- Janine Benyus
- Micheal Pawlyn
- Ann Thorpe

Biomimicry does not address all the necessary sociocultural issues involved in urban design. Intrinsic intelligence of nature A sustainable product should be able to adapt to needs and conditions

### Biosemiotics
- David Abram
- Gregory Bateson

Building a bridge between biology, philosophy, linguistics and communication studies. A study of representation, meaning and sense. An ecology of mind. Language and communication.

### Embodied cognition

The mental action or process of acquiring knowledge and understanding through thought, experience, and the senses.  An ecology of mind? Cognition as a mean of engaging with the world.

### Phenomenology
- David Abram
- Merleau-Ponty
- Edmund Husser

The Spell of The Sensuous.
Phenomenology of perception. Every living thing are human cells included irrevocably stitches in the fabric of the world. Gives the possibility of a sentient world within that world.

### Material culture & Ecological anthropology
- Tim Ingold
- Gilbert Simondon
- Michael Schiffer

Studies the network of relationships between human beings and their environments, including the materials, forces, circulations and the energy, all of which make life possible. A material as a becoming, a life proper to a matter. Mind and body as one.
Theory of individuation is that one should understand the generation of things ads a process of growth, as an ontogenetic process.
A Material Life of Human Beings. An important feature of human life is not language but the relationships which take place between people and objects. Materials of life as an active participants of our future. Material perdure. Follow the material.

### Question

>How could I develop an ecology of mind in the process of a material design, as a way to create a living symbiosis strengthening the relationship between entities?

Can a living material make you more responsible about the natural environment? Can an inorganic material replace an organic material to generate that proximity and responsibility? What benefits do living ecosystems give you?  How can something that is not living give you the same benefits a living ecosystem can give? Do inorganic environments give you benefits?

### Objectives
Create an emotionally sensitive design, using emotions to trigger a change in the morphology of the object, developing an attachment over time. Acknowledge the potential of a dynamic nature. Create a material able to respond to direct interactions (a living material), eventually adapting its behaviour to environmental challenges found in a ecological and the urban environment. Redefine a beneficial interactive communication between an a part (individual) and a whole (environment).

### Methods
- Collect and compare data from an experience between an individual and a living environment.
- Experiment with the changing state of those living and non-living materials to understand possible ways of responding.
- Translate or transpose this dynamism to an inorganic context to observe its potential benefits to well-being.
- Compare with data collected from an experience with an artificial environment with the one from a living environment.

Pathways of investigation to understand a symbiotic relationship evolving along the process of making:
- States of matter to express textures
- Morphology to express form
- Pigments to express colour

### Possible actions and tools
Prototyping the material to prove the viability of the statement. Mapping the haptic to make this communication method between systems more tangible. Storytelling through the use of emotions to make it available to anyone


### References

Cilia by Richard Beckett

Shuhei Hasado in HAPTIC

Biological Atelier by Amy Congdon

Silk Pavilion by Mediated Matter Lab

Bacterial dye Jeremie Blache

------------

## 3| Final presentation term 2
<object data="{{site.baseurl}}/resources/CatalogueOfInvestigationsNanoTech.pdf" type="application/pdf" width="60%" height="720">
  <p>Alternative text - include a link <a href="{{site.baseurl}}/resources/CatalogueOfInvestigationsNanoTech.pdf">to the PDF!</a></p>
</object>

<object data="{{site.baseurl}}/resources/CatalogueOfInvestigationsPHOTOGRAPHS.pdf" type="application/pdf" width="60%" height="720">
  <p>Alternative text - include a link <a href="{{site.baseurl}}/resources/CatalogueOfInvestigationsPHOTOGRAPHS.pdf">to the PDF!</a></p>
</object>

### Aim of the catalogue
**1** | Understand what mediates this phygital conversation and how it is mediated
**2** | Explore the physical manifestation of the digital
**3** | Analyse the implications of the digital
**4** | Crafting the digital versus the physical
**5** | Mechanisms to understand implications
**6** | Projection into the future

### Key topics
>E-waste, Mass-consumption, Material culture, Visual culture, Phygital.

With the digital world revealing itself evermore within the physical, the layers of abstractions of the digital are multiplying, making it more difficult to understand the material culture that trying to follow the fluid nature of the digital culture. The constant flow of new data in the digital space is entirely redefining the space. However the physical tools supporting this space are the same with the only difference as the number of connections made between
components. Those complex hidden connections result in a disconnection between the interactions and their implications. Humans are no longer able to relate to the digital and physical consequences of the growing digital world. Depletion of abiotic resources is an economic and environmental problem that will depend on our interaction with future technologies.

>50 million tonnes of e-waste are produced each year of which only 20% of global e-waste is formally recycled. This could be more than double to 120 million tonnes by 2050.

### Question
How will humans engage with the physical manifestation of the digital interactions of the future?

### Intro to project
This project is an exploration of the materials responsible for the interactions with two separate world, yet dependent of one another, one as the abstract digital and the other as the tangible physical. The topics of electronic waste and material culture are the starting point for this exploration. It will carefully navigate between those two spaces to try understand the physical manifestation of this digital manipulation, and try to redefine the conversation between the two. The goal of this project is to offer insights on the current correlations and mediators between the two spaces, to engage and challenge the meaning of current interfaces and shape more responsible interactions. The power of material identity and its political implications have the potential to redefine the interface of the future now.

To what extent will interactions be physical or digital? What form will take the materials that mediate those conversations? What will redefine our interfaces? What will our interfaces be in the future? What will the physical manifestation evolve into? Will humans be able to engage the tangible in abstractions?

### 1 - Understand
**A** - What objects are mediating this conversation between the physical and digital environments?

Cultural identity of a material.

Looking at the meaning of past and present artefact allows us to better understand the shift in cultural significance. The materials used in our everyday life, representing today’s digital networks are going through constant shift in values and meaning.

Material artefact such as and analogue camera, a digital camera, a analogue filming camera, a disc record, or a music player are now objects of the past, unable to adjust to the present digital temporalities.

The meaning has changed over time with the transition towards digital space. The value of such products is not seen through the quality nor the identity of the raw materials used but is now defined by its time efficiency to complete a demanded task and to provide instant feedbacks. Demands are rapidly changing, the digital transforms and the physical attempts to follow.

**B**- How are  interactions  mediating the conversation between those two spaces?
Today, smart phones comprise all those tools in one object. A letter, what used to be our only mode of communication, is now left with a possibly sentimental value only. Now voice messages are addressed to the physical and digital object. A analogue camera is now replaced with digital cameras along with smaller integrated camera in smart devices, allowing one to rapidly communicate rapidly those information as a part of the digital network.
The demand for rapid feedbacks results in short interactions. Photography based social platforms (Instagram), or message based social platforms (Twitter), and music platforms (Spotify) all are accessible in a few touches digitally, replacing the past artefacts.
The way one communicates information can result in the physical and digital environments. We have dematerialized the meaning of such artefact and in turn have rematerialized the meaning of such interactions.

### 2 - Explore
How those interactions will be redefined?
The interactive experiences that one has with a physical product are defining the meaning of such  interaction. Limited to a few short physical gestures , the hand in contact with the shiny surfaces of the digital device is the only direct physical experience of the digital interaction. This example is defining the value and meaning to such material. The digital products, services and experiences created have the power to define the material identity. Defining what it means has political implications that have the power to shift behaviours and perspectives.

The information given as a result of this interaction with the physical product and environment is defining the kind of digital space we desire and in turn the physical manifestation of those are created, creating a feedback loop mechanism.

What will be reshaping the interactions with the digital space?

Today individual cultural identity is partly formed digitally and is translated physically with object as belongings to support this semi-fluid identity. This is supported by the material culture.

Temporality of the digital. The developing cultural alternatives allow one to see how we engage with different spaces and times within our digital ecosystem.

### 3 - Analyse
What are the implications of the digital ecosystem. Variables defining the nature of this ecosystem will help us understand the different ways of the digital manifesting itself.

Energy, water, raw materials are variables needed to construct the infrastructure of the digital space, often put aside far from sight as they highlight the frictions of this digital system.

Culture, memory, scale, location, movement and time are other variables used to define the experience of time is now mediated by physical digital devices, giving different ways to experience and understand this variable.  There’s various indicators of time and time scales  that merge with the digital and physical experiences. These enables better connectivity, accessibility, and time efficiency

The use and creation of web pages, social media platforms, applications all feed into the need to materialize this space.

### 4 - Physical / Digital
In reverse, how can the electronics industry be dematerialized, to avoid the hidden implications of the digital mentioned? Are the actually gain from the use of digital worth the physical irreversible consequences?

Different terms are now used to refer to this new space created in between the physical and the digital. The term physibles refers to data objects able to become physical. Phygital is creating an experience using technology to bridge the digital and physical. The Internet of Things (IoT) refers to something that connects physical and digital products or objects with the internet. These three terms all mean something different but have all in common the conversation in between those two spaces. What is the nature of this two ways relationship?

What are the processes taken to create the digital versus the physical?

When navigating online we distance ourselves from physical world. How far will we  try to separate the real from the virtual? Should we try to link the physical with the digital?

### 5 - Taxonomy
Deconstruct the physical manifestation to understand the mechanism of the digital in relation to the physical.

As a way to deconstruct the physical form of the digital and engage in a conversation with the recycling of e-waste, I created a taxonomy of obsolete electronics.

This experiment has revealed the many materials connections to create a device able to reach the digital ecosystem. 30 rare Earth elements are found in one iPhone, with about 2 of them covering 40% of its mass: Aluminium and Iron. Aluminium is one of the most abundant metal on Earth but its process of extraction is devastating the natural environment. It is mostly used to cover the more fragile parts and keep the smallest pieces  together.

How to create a measuring system between physical and the digital ?
Can we compare and measure between physical and abstract materials along with the values attached to it? Use the measured aesthetic value and the materials used to make the digital device functional, to compare with one another.

### 6 - Projection into the future
What will be the impact of my intervention?

If an object is currently representative of digital identity and interaction, what will this interaction look like in 2050?

Would object be representative of environmental/ physical changes which happened in consequences of the development of digital?

Will our interactions with the digital be defined by one variable?
Will the material culture be redefined by the variables of the digital space or, the physical space or the phygital space?

What if social media platforms became tangible? What if you could physically and directly see and sense the digital as part the physical? What if you cannot differentiate what is physical and what is virtual?

Show what those changes in behaviour would change in the production and interaction with the digital would look like in the future.

How will we converse between the two ecosystems in the future?

What if the physical could be translated into the digital to generate a new phygital conversation?

What if the Aluminium extracted from the Earth could be achieved digitally only without using natural resources from the physical world? Could we separate the physical with the digital ? Will we live only for the digital? What would be the socio-cultural consequences?

Could we recycle E-waste into the digital ecosystem?  E-waste is not pollution nor waste, it is a vital resource.

>It is 2050.

What if there were no dependence over the physical world to produce and support the growing digital space?

What would an IoT using E-waste in Barcelona would look like? How can we dematerialize the electronic industry? What will be the consequences of IoT?

What if the digital became a space for waste generated by the digital interactions?

What if you had to buy the by-product to get the product? A product bought with its by-product?

------------------

REFERENCES

[Human Face of Big Data](https://humanfaceofbigdatafilm.com/)
[Andreas Gursky, Amberg Siemens](http://www.andreasgursky.com/en/works/1991/amberg-siemens)
[Edward Burtynsky, Anthropocene](https://www.edwardburtynsky.com/projects/photographs/anthropocene)
[MIT's Sourcemap](https://www.media.mit.edu/projects/sourcemap/overview/) [Supply chain mapping](https://www.sourcemap.com/)
---

#### Formafantasma

Formafantasma - Ore streams, Planned obsolescence - 2019 - Broken Nature XXII Triennale Milano
The videos span from a collections of historical and contemporary case studies of planned obsolescence to short clips illustrating the basic steps of e-waste recycling.

"**Obsolescence of desirability** — or stylistic obsolescence—occurs when a product’s style is altered in an effort to decrease the perceived desirability of older, less fashionable items, and make new purchases more frequent. Many products are desirable for aesthetic, rather than functional reasons.
The impact of planned obsolescence will become increasingly dramatic as the Internet of Things continues to expand. **In 2015, the** **US Federal Trade Commission** issued a report announcing that in 2009, the number of “things” connected to the internet surpassed the number of people for the first time in history.
Traditionally long-lasting products like fridges, washing machines and ovens now feature digital interfaces in need of constant updates, which are increasingly difficult to manage in objects that are interconnected.""

"According to the United Nations, 44,7 million metric-tones of e-waste were discarded in 2017. While there are efforts to improve the recovery of metals, 80% of the West’s e-waste is shipped illegally to developing countries where electronics are disassembled in poor working conditions—or ends up in landfill. Only 20% makes its way to the appropriate recycling facilities. Electronic waste is considered especially valuable due to the use of precious metals such as gold and silver in the production of chip boards. One ton of circuit boards is estimated to contain 40 to 800 times more gold than one metric ton of ore."
*To watch the full version of the system of recycling please visit: www.orestreams.com*

<https://www.formafantasma.com/filter/home>
<https://medium.com/@ava411/the-future-of-phygital-spaces-c5ae6f9855f>

-------------------

#### Automato Farm, 'Politics of Power'

"With a growing number of networked and autonomous objects as well as the outbreak of fields such as « the IoT », communication protocols used by connected products are increasingly important as they act as the network’s backbone."

"In every existing network – be it machine or nature, **rules are established in order to determine its structure, hierarchy, and the way the communication will be synchronized between all the actors of the network.** But who and what criterions will define this power hierarchy? Products and networks are inherently embedded with ideologies of the designers, engineers, and other stakeholders who shape their trajectory along the way."

"The project looks at **how a mass-manufactured product** – although developed for a precise and unique purpose – **could behave differently depending on the nature of its communication protocol and how the design of the product itself could reflect these hidden logic and rules.**"

"experience the hidden politics of networks in an everyday life products through an electrical system."

http://automato.farm/portfolio/politics_of_power/

----------------------

## Reflections and points of improvement
Along the way of term 2, I have been going back in loops, redirecting a few times my project as a way to find and reinforce the reason for taking a specific direction and approach to the issues. Whenever I had made a small step forward in making a decision, such as the idea of biological screen using biomimicry or redefining the sensorial identity of a digital device, I have been questioned again and again on the reason that is pushing me in those directions. It has been extremely challenging and frustrating to me as I found it was obvious but I guess what wasn't so much was how it is personal to me, and the reason why I can find it so difficult to give a specific example to describe what is it I am tackling. These loops have taken a lot of time and energy for me. I felt I was starting from the beginning every time someone would ask me what is really the purpose and why I want to look at problematics such as electronic-waste, mass consumption encouraged by material culture, etc.

### Feedbacks on presentation
The many feedbacks I got during the final presentation for this term were very helpful and clear. Some key points were made:
- Analyse different cultural actions in relation to the cultural object I have chosen to look at.
- Project as a product(machine, object) or process (gestures, learning, interactions)?
- Unwrap history of Photography to project myself in the future
- Am I creating a camera or a new type of interface?
- what content would this interface generate? would it generate?
- In design timeline, move between the digital and physical to reshape the object (phases of construction and deconstruction like I have started)
- dematerialise tool medium to materialise
- explore alternative materials in the physical
- reveal obscure element to light
- play with the imbalanced use of specific variables (such as time, space, location)
- start playing with sensors I have discovered in my experiment
- translate analogue experiments digitally
- redefine E-waste and use its potential?
- who are the people I am targeting?
- aspect of consumption missing in my explanations
- building new artefact (means redefining a whole industry)?
- what is this new object proposing? (use less resources, affordability, accessibility?)
- Where will I intervene in this process of production and consumption?
- Where am I proposing change?
- Suggest what will be photography become (in relation to change in material culture and consumption)?

And more fundamentally
- Build support for idea during term 3 by prototyping
- Open conversation to the outside (workshop?)
- Provide proof of concept using next term
- Take advantage of Fab Academy for my project
- Weekly reframe my core questioned, it should keep evolving
- What is the vehicle of my intervention?
- what is my vision for alter and the end of this master?

Following those critical feedbacks, I have clarified my area of intervention in the process of digital devices production and consumption using photography as the medium of communication, and tried to establish a first plan for the next 6 weeks.

This project will intervene between digital devices, their physical uses and the medium it produces.
Further explanations on the vision of the project and the detailed planning of the actions I will take in the next 6 weeks can be found in [Third term](https://mdef.gitlab.io/julia.danae/project/Term3/) page in Project. The plan will be kept updated as the weeks go past.

![]({{site.baseurl}}/term-2-presentation.jpg)

![]({{site.baseurl}}/term-2-presentation1.jpg)

## Fab Academy input in project
I will take advantage of the Fab Academy to experiment with the materiality of the components collected from obsolete digital devices as a first step. Then once I have a selection of components, I plan to play with the variables to obtain different visual results.
