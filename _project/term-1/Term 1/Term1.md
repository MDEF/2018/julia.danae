---
title: Term 1
period: 09 April 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/VisionLatentCommunicationA2Print.jpg)
<span class="text-small">*Graphics representing areas of interests*</span>

### What is Latent Communication?
An exploration of a potential form of communication aiming to empower active decision making as part of the processing of a set of data.

### Why?
Technologies have constructed an endless flow of micro-decisions, which acts as a nutrition to those machines and a distraction to the content of those information. A systemic connectivity could build a tool for expression and investigation for people to communicate with this other element and react to it. This new type of communication would aim to give people a genuine connection to hold on to and give more responsibility in relation to the given information and environment. How to design by looking, not only at the object, but at the matrix of rules and relationship in which the object finds itself?  How to make visible the latent communication potential? Could the design of an interplay between new specific elements of an environment create a network for a type of communication to trigger reactions and awareness? This type of communication can be seen as a ‘growth medium’, allowing flexibility and creating a transitory space for an idea to grow and be translated to the specific context. A sort of in-between time-space for contemplation followed by a concrete systemic connectivity between two systems.

Latent Communication aims to reveal the translation and communication of information happening in between the organic and technological systems in an urban setting. It aims to use communication as a an instrument to reveal unique connections between elements of different systems as a way to unify those.
The different levels of connection between the human and its environment varies in scale. Some of those connections cannot articulate an obvious communication for the human to sense for being too far, too small or using a different set of signs to communicate within this system, keeping our body from sensing it and respond to it.

Digital citizens with a digital nutrition. Digital age segregating knowledge and place. How to navigate in the information overflow. Lost information in the digital translation. Every experience is mediated by technology. Intelligence of nature changed by technological innovations along with its value. How is information processed in between those realities?

### Points of attraction
Interested in the growing, the breathing, the physical, the organic, the sensing, the communication, and the complexity of simplicity of things, in design and life? Movement, constant evolution, change, interaction, contraction and expansion, regeneration, connection, a cycle implying transformation of something into another in order to achieve the loop. Growth in the economical context and in the biological context.

Looking for the ability to respond and adapt to environmental challenges. Some examples of systemic challenges in nature are  access, awareness, production and distribution... Acknowledge the active and dynamic natures of animate organisms.

A reality as what is agreed, constructed, factual, physical, a common ground of understanding seen as an outer world. Then there is also the realities of our individual bodies, subjectively defined, proper to each human being. Through those accepted truths and norms, a consensus is achieved about precedent hypothesis and results. Once this is accepted by society, it is deeply anchored in our beliefs of what is reality.

Understanding and imitating functions of living organisms as part of a self-sufficient network of organisms.

Architecture as unfit built environment? Dominant logics using blunt instruments to construct a specific future. Traditional architecture  designs for one kind of future, once built might already be obsolete with today’s urban rhythm. Modernist architecture and ambitious master plans as a response to the housing crisis as early as in the 60s to the 80s are temporary solutions.

![]({{site.baseurl}}/InsightsOfAJourneyA2PrintCut.jpg)

![]({{site.baseurl}}/InsightsOfJourneyLegendA4Resize.jpg)

----------
‘Technological Nature: Adaptation and the Future of Human Life’ 2011, Peter Kahn

‘The Eyes of The Skin’ 1996, Juhani Pallasmaa

‘Metabolizing Complexity’ 2017, Ben Cerveny

Urban Futures’ 2016, Thomas Ermacora

‘Life 2.0’ ‘Life 3.0’ 2017, Max Tegmark

‘The New Normal’ 2018, Nicolay Boyadjiev

‘Recoded City: Co-creating'

‘Towards a Semiotic Biology: Life is the Action of Signs.’ 2011, Kalevi Kull, Claus Emmeche.

‘The Spell of The Sensuous’ 1996, David Abram

From Bacteria to Bach and Back’ 20, Daniel Dennett

‘Speculative Everything’ 2013, Dunne and Raby

‘Tomorrow’s Thougths Today’ Liam Young

‘Biomimicry is innovation inspired by nature’ 2002, Janine Benyus

‘Biomimicry in Architecture’ 2011, Michael Pawlyn

Mediated Matter, MIT Media Lab, Neri Oxman

‘Project Persephone’ 2016, Rachel Armstrong

‘Grands Ensembles’ movement

‘Million Programme’ project

‘Fun Palace’ 1964,Cedric Price
