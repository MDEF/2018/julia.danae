---
title: Term 3
period: 14 June 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

<span class="text-small">*Latent Communication development*</span>

## April 2019
> How will humans engage with the physical manifestation to the digital interactions in the future?


### Why

Three reasons that link to each other.

- **Depletion of abiotic resources** on which we greatly depend to sustain the current digital culture dependent on materiality.

  Environmental issue and social behaviour.

  Inability of this by-product, as the phenotype of a data, to adapt to the changing state of matter of the digital.

  ​	STATISTICS

- **The haptic is getting lost.** Digital interfaces reshaping our senses and ourselves.

- **Disengagement in the physical world**. The feel of physical environment is getting lost.



Communicate the implications of the dependence to the digital world, which is invisible to our eyes.

To interpret data from digital images into matter.

*To create tacit ontology totem to find an alternative way to communicate.* To develop an attachment over material/ physicality of the world (whether digital or physical).


### How

**Exploring how haptic could be redefined, by revealing the power of material identity in relation to digital interfaces.**

Haptics and emotionally sensitive design

Intangible capturing tangible for intangible. Dematerialising to rematerialize WHAT

#### 	Intervention

- Measure data consumption in a material form (ex: weight, scale, pressure, texture,...)

- Embodiment of data

- Changing state of matter of data to experience and create a new nature of data

- Material shaping process of information exchange (interaction generating material and digital feedback)

- Enabling emergent behaviours towards haptic design


### What

**Reveal the beneficial possibilities for a haptic lead interaction, to influence the choice of materials for the next generation of digital interfaces.**

Audience:

- Materials targeting designers and production industry

- Interaction and use targeting digital users

This form of haptic interactions will create a different form of information - a *tactiograph* as one of them .

Through a research based process, Latent Communication aims to reveal the physical implications of the interaction one has with data phenotype, while shaping a new form of communication using the sense of touch.

A haptic word. The sense of touch has been shown to provide a strong understanding of the environment when other senses are unable to provide enough information. Latent Communication is expressed in the form of a material, able to provide a direct understanding of the physical and material implications of using a digital interface to communicate.

Ultimately it is suggesting a tangible alternative to communicate with another, too far to touch but close enough to connect to.


### Touch-Driven Design

I realised I need to define what is touch in order to be able to start changing the way we use touch.

What does touch mean today? Before looking at any dictionary, I tried to define from what I know as touch. A touch involves another element, making contact as part of the physical world. "A sense of touch", "touch on subject", "caress, strike, push, reach", "keep touch", "feeling touched", "get in touch", all these expressions define the many meanings associated to the noun and verb. Touch is first understood as an action that can be associated to intimacy, however it also means to share and communicate, it implies a sense of responsibility and can be emotional. It is a way to approach matter in general, whether it be biological or artificial. Today, touch is limited to the contact of a screen as a way to do all of those things. I believe it has more to offer in the ways we can understand our environment and needs to be redefined in response to the new digital revolution that has changed it in the first place. We now touch screens to touch people.

Shape-changing interfaces / Ergonomic / Live motion / Interactive interfaces / Material properties / Auxetic materials?

[Input device MIT](http://academy.cba.mit.edu/classes/input_devices/index.html)
[Example from MIT](http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html>)


### Exhibition ideas

**What do I want people to take away from this interactive installation?**

**When I enhance body with new tool, what is the conversation that appears?**

**How I create tools for non verbal language conversation?**


1.Body ability to relate to material

2.A dialogue using empathy

3.Conversational gestures (body movement) - distant vs. intimate

4.Haptic emotions (experience feeling)

5.Digital haptic feedback

6.Communication process

7.Chain of reaction (different actions and reactions for different connections)

8.Interact with digital and with physical

9.Physical input and digital output

10.focused purpose of interaction

11.post-output to analyse user's behaviour



Mechanical motions variables:

- rotary (torque)
- oscillating
- linear (slider/rail)
- reciprocating
- irregular motion (bees fly)



**Capacitive sensor (input)**

​	Measure pressure, surface covered, switch

​	Different materials for different motion? or pressure?



**Digital and physical output**

​	Processing or LED pixels or visual of touch (spirograph?)

​	Motors and gears for movement of materials, change in state of matter


### Reflections

Small prototype for small tested interventions

Large movement with not much effort

​	Bigger more flexible and smaller more rigid.


Use properties of material

Pursue what feels most unusual

the movement as part of the interaction is what will matter most

Strategy: Small electrical shock?



Touch a material that is still or moving, cold or warm.

Your touch is being communicated thanks to the installation.


Obscuring vision

​	representations of a geometry that is different from what you will feel when touched (illusion).

​	numbing vision with light game installation


Materials choices deciding on the equation.

Play with intensities of feedback?

Raw data - coordination and pressure.

A series of gestures in contact with material are getting captured - START and PAUSE (time scale)



**Visuals complementing the installation**

- series of touch communication developed (images)

- Title, intro with question
- Narrative detailed project explanation (Catalogue format)
- Video screen/projection (capture example of interaction, explain visually problematic, key words, by-products, intervention)
- One blueprint technical drawing
- Relating to weak signals
- Exhibition



### 129 grams of matter

An Iphone6 of 16GB weighs 129 grams and about 37 different materials to be produced.

- 24% Bauxite, **Aluminium ore** (31.14gr) (Guinea) Image source: <https://www.bloomberg.com/news/photo-essays/2015-10-09/guinea-s-dawn-overcoming-hardmen-a-commodity-crash-and-ebola>

  Wikipedia <https://en.wikipedia.org/wiki/Bauxite>

- 14.5% **Iron ore** (18.63gr) (Australia) Image source: <https://www.ibtimes.com.au/brazil-surpass-australias-iron-ore-production-3-years-1449281>
  Wikipedia  <https://en.wikipedia.org/wiki/Iron_ore>

- 17.1% Copper (7.84gr), Cobalt (6.59gr), Chromium (4.94gr), Nickel (2.72gr)

  - **Copper ore** mineral, Chalcopyrite and Bornite (Chile, Peru) Image source: hiverminer.com
    Element periodic table <http://www.rsc.org/periodic-table/element/29/copper>

  - **Cobalt ore** (Zambia, DR Congo) Image source: <http://www.miningnewszambia.com/zema-receives-mushindano-cobalt-mine-eia/>
    Wikipedia <https://en.wikipedia.org/wiki/Cobalt>

  - **Chromium ore** (South Africa, Bushveld complex) Image source: <https://www.thermofisher.com/blog/mining/infographic-8-fun-facts-about-chromium/>
    USGS Mineral Resources Program <https://pubs.usgs.gov/fs/2010/3089/pdf/fs2010-3089.pdf>
    Wikipedia - Bushveld Complex <https://en.wikipedia.org/wiki/Bushveld_Igneous_Complex>

  - **Nickel ore** (Autralia) Image source: <https://www.tomra.com/en/sorting/mining/segments/non-ferrous-metal-sorting/nickel>
    Metals prices - Nickel <https://www.lme.com/metals/non-ferrous/nickel/#tabIndex=0>

    International Business Times article <https://www.ibtimes.com.au/nickel-exploration-activities-continue-boom-australia-1446185>>

- 15.4% **Carbon** (19.85gr) (USA, Russia) Image source: <https://atlasofthefuture.org/video-do-you-speak-carbon/>

  Wikipedia coal [https://es.wikipedia.org/wiki/Carb%C3%B3n](https://es.wikipedia.org/wiki/Carbón)

- 6.3% **Silicon** (8.14gr) (China, Russia, Brazil, USA,...) Image source: <https://en.wikipedia.org/wiki/Silicon>

- **About 30 other rare earth elements...**

  - Cerium (solvent to polish touch screens)
  - ...


More links:
[Digital devices ownership in the US - statistics](https://www.pewinternet.org/fact-sheet/mobile/)
> Ownership, dependency and reliance.

[Global amount of users, active on Internet in April 2018 - Statistics](https://www.statista.com/statistics/617136/digital-population-worldwide/)
> North America and Northern Europe both ranking first with a [95 % of internet penetration rate] among the population. The [countries with the highest internet penetration rate worldwide] are the UAE, Iceland, Norway, Qatar, Bermuda, Andorra and Aruba with a [99 % online usage rate].

[Rare earths](https://www.businessinsider.com/rare-earth-metals-elements-what-they-are-2019-6?IR=T#the-15-lanthanides-found-in-the-second-row-up-from-the-bottom-of-the-periodic-table-with-atomic-numbers-57-to-71-are-rare-earth-elements-the-other-two-are-scandium-and-yttrium-1)

[Materials in a smartphone](https://www.statista.com/statistics/270454/top-10-materials-in-a-smartphone/)
[Raw materials in Iphone 6](https://www.visualcapitalist.com/extraordinary-raw-materials-iphone-6s/)


### Analytical form of design action

This idea of a design action was entirely analytical and is missing the emotional aspect to represent my project.

After researching and gathering more information on the identity of the raw materials used to make an iPhone, the idea was to calculate from the percentages of mass for each materials to translate into pixels to form a *tactiograph*. In this design form, touch is translated as a value to reflect on the implications of our touch in relation to the materials identities. How many people have touched this matter we hold in our hands? How much energy has been put in this matter? Why touch? What is the potential of touch? Touch as a form of energy. The idea was to use raw materials as a visual representation of the evolution of the meaning and use of touch when matter is manipulated in the process of making a mobile phone. This touch shaping the value could be traced in a journey. Starting with a rough first contact (mining, extraction from the Earth's crust) and its value increasing in the process as it becomes a product on its own to then be sold on the market for production (sell and buy precious minerals, selection of quality,...).

The installation could be done in a way that one individual touches different types of matter at a time (capacitive sensor + matter) = one value (pressure) for every given material.

The power of touching in a digital world.

### Vision of the future

By projecting a user experience into a future materiality, I am able to focus on what are the needs and goals for the  future user persona I am creating. The vision I have of the future, in relation to the next generation of digital interfaces, is focusing on the materialisation of today's digital interactions. The nature of interactions today is limited and defined by the crystal surface of a universal touchscreen. But what if a user has the opportunity to define and create it's own form of interaction? Able to choose the properties of a material according to what information needs to be communicated. This information will be defined by your own manipulation of the chosen material to create a message unique to each one of the user. The interface is no longer just a tool fulfilling it's role but becomes an object with a capacity for emotional trigger. I envision a future in which digital interfaces are defined by the matter that is forming today the infrastructures of our cities. A man engineered matter capable of hybrid methods of communication, on one side digital and the other physical. I envision any citizen as a digital user, able to communicate information individually and collectively, to one another. A connectiveness made of the remains of our old city's infrastructures, developing into emotionally intelligent information systems by the human touch of matter.  

What will become those types of interactions in the future? What it is to write a text message today, will be created by the different touch of specific message.

### Interactions and materials correspondence

- Vibration as a response - interactive communication.

- Button as a key - pressure to open - turn on/off, open or close device.

- Swipe as a direction - movement to aim for something else, for change of information display.

- **Text** using digital keyboard/voice dictation - handwriting recognition? movement and pressure

- **Image** using a digital button/tap/click - positioning on interface constructing the picture



| inputs                   | interactions                                                 | materials                               | properties            | outputs                                                      |
| ------------------------ | ------------------------------------------------------------ | --------------------------------------- | --------------------- | ------------------------------------------------------------ |
| short time pressure      | A hybrid (digital & analogue) button to take a picture       |                                         | resistance, hardness  | Compose a *tactiograph*.                                     |
| short time pressure      | A series of tap in different location/rhythm to compose a message | stretched fabric                        | elasticity?           |                                                              |
| movement + pressure      | A swipe to get access to information                         | hairy soft material (3D, ex.Cilia like) | smooth uneven surface | Scientific story of material's ID used today in this type of  interaction - glass screen. |
| located lengthy pressure | A vibration to read a message                                | Fabric attached to micro springs        | bounce back to shape  |                                                              |
| **new interactions**     | 1. button >                                                  | 2. series of tap >                      | 3. swipe >            | 4. vibration >                                               |



---------------
## March 2019
### Project time management

<iframe width="60%" height="720" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSJis78NrQKo0ecfdg09da0yiacGYTb0z097ZttJFaqHMyAjp1JSqfoYGM7lZaXzC3BvwbxR9mknBXI/pubhtml?widget=true&amp;headers=false">
</iframe>

### Planning system
This plan is divided in different categories with different speed for completing. The VISION is essential to reinforce the structure of the project. It aims to clarify what I imagine as the outcome being for the end of this master and what I would want this outcome to evolve into past the master. To do's NOW is a category that will keep being updated weekly, I have now shown the first few days of to dos. It is made of quick or small actions that once done, should inform what will be the next steps short steps. They ensure I keep making to reach my goal. Answers to DEFINE is another category that consists more of researching deeper certain key points that will ensure I am going in the right direction. These should decrease through the weeks and the making should increase. It is also about answering questions, then making decision that will generate smaller questions to make another decision and so on. METHODS for process is ways I could start making, some will have to be done at the same time and converge at some point. Others might be put on the side if found irrelevant as I go on in the days.

### Goal

*Latent Communication* envision an alternative process of photography that reveals the materiality of components used today in digital devices and cameras.

This new vision of photography goes further than ocularcentrism and develops a focus on the touch and physical interactions around the instrument that is mediating the process of capturing moments (physically - lenses and digitally - screens).

This project aims to capture and reveal a new nature of interaction between a digital consumer and its environment, by looking at the materiality of the tool that is enabling this process.

The component that is responsible for the scale, the focus, the format and the aperture we use today to communicate those images is defined by the choice of lens. Playing with variables, such as light, scale, position and movement can affect the result the lens creates and the way one decides to manipulate such tool. This project will result in a new instrument of photography and a series of experimentation resulting in the use of it. A hybrid process will have been tested, moving between making the physical and making the digital. The type of photography that will be defined along the way of experimentation, will be used as the medium to represent the result of this process. The outcome will allow people to try for themselves the process of bouncing between digital and physical in the process of making a photograph, and observe how the results obtained inform the way they can use the instrument. It would open up the possibilities to create variations of the same moment, influenced by one's interaction with such artefact and therefore alter desires for the future.

### Intervention proposal for term 3

Following the end of second term review, I have drawn the steps of the process of production and consumption of a digital device, as a way to find where exactly in this industry I wanted to intervene. This also allowed me to understand what would be the consequences of my intervention in the timeline of this instrument.

Steps

I am now starting the defining the prototype to prove that changing the nature of our interaction with the tool that mediates between the digital and the physical, can alter the type and format of the product it generates in the process of use and end up redefining the kind of instrument/digital device we ask for. Photography has rapidly evolved through time and is today a method of communication used by every digital consumers, to capture and share moments in specific time and place. Light waves have been increasingly privileged in photography, putting aside every other senses in the process of capturing a moment. This evolution has distanced the physical from the digital manipulations, resulting in a misunderstanding of the inter-dependence between those two spaces. I am now asking a few questions that my prototype will need to answer. Could touch, 'the mother of the senses', be the new sense used to capture ephemeral information? Could we create a new method and format for photography, changing the way we communicate and use this a digital device capable of photography? How could one record, send, share touch photographs? How would one want to touch information?

Outcome

I want to create a photograph of touch. What would a photograph of a moment would look like with touch as the input rather than light? I now need to rapidly choose what type of sensor I want to play with, in relation to touch, to understand technological feasibility. Then start creating the tool that allows this process. The first sensors that come to my mind are pressure, position, movement. Then I will use the data I get from the use of a specific touch sensor to translate this experience digitally and see how it would retranslate physically too.

For the final exhibition, ideally I would want people to be able to experience this interaction and create their own photographs to see and feel.
