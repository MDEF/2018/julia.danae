---
layout: home
---

![]({{site.baseurl}}/images/P1104109.JPG)

This website has been created to share the insights of a journey taken through the one year *Master in Design for Emergent Futures*, organised at the Institute for Advanced Architecture and Elisava School of Design and Engineering. As part of this master I will be taking part in the Fab Academy, a Digital Fabrication program directed by Neil Gershenfeld of MIT’s Centre For Bits and Atoms, based on MIT’s rapid prototyping course *How to Make (Almost) Anything*.

I will be continuously sharing thoughts, knowledge, tools, methodologies and experiences, all informing the development of my master project. This project will be expressed in the form of a small design intervention, targeting a specific problematic in the field of interest.
