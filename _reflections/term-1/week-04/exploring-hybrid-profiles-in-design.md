---
title: 4 | Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Oscar Tomico*</span>

This week’s exploration and reflection started with the visit of three professionals based in Barcelona.
Those encounters aim to develop thoughts over identity and career choices by comparing who I am now with those professionals and question who I would like to be.

I have discovered and experienced various skills, knowledge and attitudes.
Below you can see a diagram showing the key things I found important in those hybrid profiles, following with a more detailed description and reflection over the discussions we’ve had.

![]({{site.baseurl}}/HybridProfiles.jpg)

**What is a hybrid profile?**

A hybrid profile explores the interconnection of different practices within the realms of technology, science, design. It enables a better balance between skills and knowledge to create work that will be of better relevance to current status quo. Furthermore, I understand it is tackling challenging problems today in a more tangible way, looking at more than one method to try to solve those. It allows a better adaptability to this complex environment.


### Ariel Guersenvaig

IDENTITY CHOICES

Started with the field of Design and is now a researcher on ethical digital transformation and teacher in ELISAVA.
How?
*Web designer > interactive design > human computer interaction - usability > information architecture > service design > design rationality > analytic philosophy - ethics*
Ariel has shared with us his career development but also personal life events for us to understand the decisions he has made.

I understand how sometimes decisions are not made only based on work but also personal context and the status quo, shaping every step of this constant evolution.

DESIGN THINKING

We discussed how in design we are mostly rational and not necessarily analytical. We base heavily our ideas and decisions on intuition. And with this intuition, we do things as a way to understand a problem and try to solve it. This method is proper to designers and creative minds, therefore it can be difficult for other fields using other methods, to understand or approve.
So how can I work within different fields using this design method? Can it be understood, justified, is it viable to other fields? Does it need to be?
In the discussion, Ariel mentioned Frank Gehry to represent a discussion between design and the designer and see how it is enabled through making and prototyping.

This is part of a process, essential to help reflect on an idea.
This way of thinking enables further questioning the role of every design results, which leads to ethics.

ETHICS AND PHILOSOPHY

Ethics is and will become even more important in the future with recent technologies such as AI developing. This thought lead us to discuss the concept of killer robots and what could be potential arguments for and against it. I can see the point of arguing its introduction in society but I also feel the urgency to question the need to create things only because we can.
Do we have to create things because we can? Should we need killer robots? How did we come to think of designing such things? If we know there’s many risky or problems that could be triggered by those, why not stop there?
Politics and economics and probably important reasons for why not. However today, especially in design we are pushing forward the importance of cultural, social and individual needs, and yet this is not powerful enough.

Is there a limit?
The only thing I understand a killer robot would do in this case, would be to move people away from responsibilities. Current linear material consumption lifestyle is already pushing individuals as far as possible from the impacts created.

Could it be legalised if there’s meaningful human control?
The “Trolley problem” is an experiment in ethics which aims to help designers to understand better ways of teaching machines to behave. The difference between letting die and killing somebody is an impossible choice for someone to make, therefore could a learning machine feeded with all contextual information see this difference and make the best decision on its own?

This kind of decision is consequential, but who to blame if something goes wrong? Wrong according to what context?
I wonder if there’s a point in trying to teach machines how to make decisions in certain context in which *we* humans are not even capable of making.
Are we trying to create the perfect human? A sort of generic god?

A large amount of selected data is given to a learning machine, it then used through an algorithm to give the answer to the question given to the machine. We can establish the data given, but will never know how the machine makes the decision to use those information.
Its unpredictability is the risk.

WHAT / HOW?

This strengthens my thought that I would want to create a language, a system that would aim to solve a problem rather than *product* . Does this language have to be three-dimensional for it to be effective in this three dimensional world?



### Sara De Ubieta


IDENTITY CHOICES

Started in the field of Architecture and is now experimenting with shoe making and is teaching shoe crafting.
How?
*Architecture > shoe craftsman > material experimentation > world of Fashion > teaching*
It was through her studies in Architecture that she first discovered shoe making profession. After acquiring the skills for traditional shoe crafting with leather, Sara explored using materials she new from architecture (such as insulation material, wood beams, plastic pavement, etc.) and shared those new skills and knowledge with other people through teaching.

![]({{site.baseurl}}/IMG_4235.jpg)

IMPLEMENTATION

What I strongly related to was her need and satisfaction in actually producing concepts. By doing, she was able to discover materials and explore how they can be used to create something.


In architecture, this approach is nearly entirely absent, I have seen and felt a huge gap between the concept and the actual implementation of this idea.
That is one of the reasons why I decided to join IAAC. I believe it will help me create a network that will allow me to make ideas become reality.

I have always thought working with people who have different approach but similar aims is essential in order  to create a successful project. However, Sara has done everything on her own, developing those designs and coming up with new ways of using unusual materials has been created only be herself. I admire this independence and wonder how different it is to work in such a way and coming to the ‘realisation’ of a project like this.


DESIGN METHOD

Again here, it links back to what Ariel was saying about prototyping and the need for creating alternative and intermediary solutions as a process. Sara has demonstrated importance the importance of this method.

“It is rather how you work a material that is new rather than a new material.” Sara

We learned how leather, as a material, is worked and how it could be done limiting the amount of waste.
Back to the discussion we had with Mette on week 2, I feel it is essential to explore possible new sustainable materials but also new ways of using them in a concept. The aesthetic of a shoe depends entirely on the material choice and how it has been worked.

![]({{site.baseurl}}/IMG_3941.jpg)
REHOGAR exhibition, Bio-based Materials by ISAAC CORES IRAGO

When we are introduced to new materials we make the same mistake to use it in the same way as the material we know. Why not give the freedom to creativity and experiment to find serendipity?

Sara also said something I found important: “it is interesting to see how different countries use different methods to solve the same problems” but don’t communicate or merge those methods to create one that could be applied in different situations, saving time for application.

One way to allow much more flexibility to adapt to various context would be to construct problem-solving methods as parametric designs; the same way designers do with Grasshopper plug-in in Rhino.



### Domestic Data Streamers
<span class="text-small">*Introduced by Pol Trias and Marta Handenawer*</span>


IDENTITY CHOICES

Started in the field of product design and are now using data for communication. How?
ELISAVA design/engineering > DDS > enabling art > interaction > languages > methods to learn.
Marta did a degree in engineering in ELISAVA and came back to DDS with a focus on trends in Japan.

“ How to define ourselves? “
The definition of DDS is constantly changing in the same way that our environment does. It is essential to construct systems and methods that can adapt to face paced changes.

A TOOL

For this final encounter, we talked about design through different communication methods, always with a playful aspect.
They use every context to create a narrative for any individual to participate in and adds to the making of more accurate data.

DDS is using communication as a tool in away that can be very powerful for change making and awareness.
However I question how making advertisement for known brands can make a difference in a people’s way of thinking and acting in their own lives.

Marta tried different things within design but was mostly interested in interactions with humans and analysing how they behave and the trends they create within our society.

Through diverse projects they learned about people coming to see the exhibition and how they understood this and design.

DESIGN VS. ART

How to define art?
Art for them was used to allow the people to be the protagonists, the artists were the people themselves rather than the ones setting up the right environment for it to happen, like Paul and Marta. They constantly play with that boundary and provoke the set rules defining design.

PROJECT FRAME

How much is going to cost a project in terms of resources? This question is asked before any project can take a start and it helps frame the project. DDS has customized a platform to collect their own data and better manage the different projects, time, money, and work.



### Define my identity

Below is a diagram selecting some skills, knowledge and attitudes that I find important and would like to develop and improve in the near future.

![]({{site.baseurl}}/MyFutureProfile.jpg)

A personal development plan is necessary in order to find out what I want to learn and plan how I will use this master to achieve this. This is a first draft but I hope it will become more specific and accurate within the next months.

![]({{site.baseurl}}/PersonalDvlpmtPlan.jpg)

### A vision of the future

I would like to discuss the idea of *future*  but for now and for this conclusion, I would like to  focus on the *vision* of this *future*.

I will try to say in a few word what I aim to be:
As a designer, I aim to explore the role of advanced technologies today and how they should be implemented in emergent design systems. A multidisciplinary approach will allow me to investigate the realms of design, arts and sciences as part of the digital and physical realities, to try tackle today’s big challenges faced by humanity.

I aim to:
- Take a holistic approach to design
- Understand the role of advanced technologies
- Construct small scale interventions to approach those challenges
- Work on multiple scales
- Create multidisciplinary methods
- Create an international network
- Test methodology to promote real solutions
- Expand boundaries and redefine the meaning of architecture today
- Become an agent of change as part of diverse fields
- Research, analyse, experiment
- Develop methods as parametric designs for adaptation
- Create a symbiotic system to reinstate living things and connect to it as part of our lifestyle.

This vision might seem generic but I see it as a method that can be applied in different ways for different contexts we might encounter in the journey of being a designer.


-------


### References

[*Design for the unknown : a design process for the future generation of highly interactive systems and products*](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products)

“We critically reflect on two existing influential paradigms of
design methodology, i.e., the ‘rational problem solving’ process and the ‘reflective
Practice’”

“The first paradigm considers design action to be an activity that implements information instead of an activity that generates information.”

“We present an open and flexible reflective transformative design process that highly regards design action as a generator of knowledge next to analysis.”

“The process supports students to transform the world, and integrate knowledge, skills and attitudes, next to offering moments of reflection.”

[*Annotated portfolios and other forms of intermediate level knowledge*](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge)

“[a]nnotated portfolios are, perhaps, a way of modestly and speculatively reaching out beyond the particular without losing grounding—and doing this with all the rigor and relevance needed to inform the invention and detailed development of new designs”
*Source: Gaver, B. and Bowers, J. Annotated portfolios. interactions 19, 4 (2012), 40–49.*

“The researcher can add knowledge value by providing annotations in addition to the artifacts”

[*Designing for, with or within*](https://www.semanticscholar.org/paper/Designing-for%2C-with-or-within%3A-1st%2C-2nd-and-3rd-of-Tomico-Winthagen/cfec7ddb0861f0b5ad32bf87f252acff8a16b2e0)

[Frank Gehry in the Studio](https://www.youtube.com/watch?v=x6m0abRv9D0)

[Artificial Intelligence and the Future of Warfare](https://www.chathamhouse.org/sites/default/files/publications/research/2017-01-26-artificial-intelligence-future-warfare-cummings-final.pdf)
