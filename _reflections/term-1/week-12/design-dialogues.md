---
title: 12 | Design Dialogues
period: 17-23 December 2018
date: 2018-12-17 12:00:00
term: 1
published: true
---

*Work presented to James Tooze (Royal College of Art), Markel Cormenzana (HOLON Design), Elisabet Roselló (Center for Postnormal Policy and Futures Studies), Ramon Sangüesa (Elisava - Equipocafeína), Lucas Peña (Cognitive Technologist), Guillem Campodron (IAAC Fab Lab BCN), Jonathan Minchin (IAAC Fab Lab BCN), Ingi Freyr (IAAC Fab Lab BCN) and Santiago Fuentemilla (IAAC Fab Lab BCN)*

![]({{site.baseurl}}/VisionLatentCommunication_A2Print.jpg)
![]({{site.baseurl}}/InsightsOfAJourney_A2PrintCut.jpg)

![]({{site.baseurl}}/VisionBooklet_A4A7LayoutCut.jpg)
![]({{site.baseurl}}/InsightsOfJourneyLegend_A4Print.jpg)

![]({{site.baseurl}}/VisionQuestions_A3Print.jpg)


## Feedbacks

- why so much rationalisation
- try use absurdity
- Be more emotional, not so rational
- put your body into one idea to experience and experiment
- stop synthetizing
- make a choice, get specific, experiment
- build a relationship with a slower process (valuing and making)
- time is valuable
- Infographics
- What biological things communicate?
- where is the conflict?
- at which level would I like to tackle the problem?
- what do I want to learn?
- what do I want to get out of theoretical skills?
- inhabit your ideas
- decide between ideas not challenges
- be critical of projects/theories/concepts I am inspired from
- read again what I wrote in the past weeks

### Reflection

In those graphics, I have tried to show visually the process I have gone through to try filter and digest the load of information and resources we have been presented during this first term. The physical presentation and layout of my areas of interest have not been shown clearly enough, and that has lead us to talk more about the fact that I need to make a decision of the specific interest I want to explore rather than discussing possible ways of applications of those interest into the forms of interventions and concrete ideas of experiments. Those past weeks have triggered some thoughts which have pointed me in different directions and areas of interest, and at every stage opened new doors for research and discovering new interests. I realise this has kept me flying in the abstract cloud of theories and philosophies, keeping me from landing to some more concrete ideas of applications that could lead me to a project in the form of a physical scaled intervention. As I have been reading and listening and writing about those areas of interests (mentioned below), I have also been distancing myself (physically and mentally) from those, trying to simplify complex ideas and applications to understand it better but superficially. In this process, I have forgotten myself. This might sound ridiculous to others but I have realised recently that I can only design something for the people by designing it for myself first. In this way, I can start looking at myself. Try to understand who I am, who I want to be, what position I want to take today and in those futures we've explored in those past weeks.

So here I am again, back to this starting point, but this time with more knowledge and tools to take action. Decision is urgently needed in order to start something that can be taken forward, but how can I do it? What has been keeping me from making this decision? Am I scared of missing an opportunity? Is it the amount of possibilities? Is it the wanting to create something new, be innovative? Am I scared of engaging myself into something specific? I feel I have been an observer of my own behaviour without being able to physically react to it and act. Like a form of 'decision paralysis', in reaction to the load of information, opportunities, situations, problems and scales at which they were looked at.

I feel the only way to get out of this state is to go back to the essentials for a bit, focusing on the things I like without necessarily trying to find reasons for it or ways of using it for a possible project. It is within those experiences that I can find ideas or realise what is truly essential to me, as a way to make a decision. Next thing I am looking to do is to find the right question that I am trying to answer through this project.

Next presentation I would like to create an experience that is more interactive and provocative, by looking at what has been done to criticise it, and suggest an alternative or improvement to this  specific state of the art.

### Look into

- state of the art
- slow technologies (movement)
- biosemiotics
- communication
- time as a mediator
- sensorial experiences
- digital information
- biology and neuroscience
- digital visualisation
- symbiotic relationships
- reflective design

### Next 6 months

- proof of concept
- explore and test ideas
- future presentation will need to give more about the existing state of the art
- develop idea
- deploy

## Choice from experience

People choose by memory and associations and those memories are built with experiences.
Emotional well-being can be measured with 'u-index' which can determine the happiness of someone by looking at negative and positive emotions, but more than that this index is an objective measurement of time. Well-being is experienced when experiencing something fully and monotasking can help reach this. We can learn to control our use of time as a way to increase well-being. So how can time be redefined? Value a series of moments as a way to experience the self? Or represent time in a different way? When we asked broad question, we understand and answer it in a way that narrowed it which means one looked at fewer factors that could help answer accurately. This selection could lead to the neglect of certain essential elements. Maybe the method to take a decision could be looked through three tasks: comparison, choice and ordered reasoning. I very often use my intuition, which to some extent erroneous, registers the cognitive ease with which it processes information but it doesn't generate a warning signal when it becomes unreliable, so I question weather I am truly processing information in a way that can help make a decision. It sort of a cognitive illusion versus the perceptual illusions.


## Starting points

### Ideas and goals

- Reinstate nature in the urban context (physical connection, highlighting necessity, understanding mutual needs).

- Create a responsive culture towards/with the natural environment. Challenge the urban rhythm with the one of the natural environment.

- Create a system to communicate between living things (plants, microbes, fungus), humans and machines(what kind? Ones accelerating the pace of production and consumption).

- Redefine the meaning of architecture along with it's role in our cities. Doesn’t it already exist? What is it and how does it work?

- Shift from a mindless consumption of real-time digital information to a active filtering of it in a slower pace.

### Connection and communication

Create a common language between human (constructed reality) - nature (organic) - computer (digital intelligence) by finding what they have in common.

What do these three systems have in common?

| Human                   | Nature                                                       | Computer                                                     |
| :---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Body,  Language, Senses | Living organisms , Sound Colours (butterfly), Light Temperature Symbols (female/male),  Fractals (one dimension (a line) and show each succeeding iteration as another line), Φ (golden ratio - fractals) | Binary code, (1,0), Code, Pixels, Light, Sound (frequencies), Algorithm |

When one fails, technology as one of the most probable one, another system such as nature could replace the interface for communication. But what happens if it is the reverse that happens?

a + x = b

An equation as a collection of expressions, when variables (x) are substituted the equation becomes an identity.

What if that equation contains a subtraction, meaning if for instance technology was suddenly missing, what is the solution?

For instance in the scenario of natural disaster, such as a hurricane, the environment can start to appear to come awake, forcing a return to our complete senses. It is in the absence of our technologies that we can rediscover the natural environment and its organic entities in a way that can be vitalising. “An experience of reciprocal encounter”. Acknowledge the active and dynamic natures of animate organisms.

Brings to a more personal scale.

What if nature was other than a stock of resources for human civilization?

Perceptions: A common agreed language to describe physical reality but the way our mind describes it is different. For instance, one could call yellow what is 'blue'.


### Embodied cognition

<https://en.wikipedia.org/wiki/Embodied_cognition#Artificial_intelligence_and_robotics>

 Is the theory that many features of cognition, whether human or other, are shaped by aspects of the entire body of the organism.

The modern version depends on insights drawn from recent research in [psychology](https://en.wikipedia.org/wiki/Psychology), [linguistics](https://en.wikipedia.org/wiki/Linguistics), [cognitive science](https://en.wikipedia.org/wiki/Cognitive_science), [dynamical systems](https://en.wikipedia.org/wiki/Cognitive_model#Dynamical_systems), [artificial intelligence](https://en.wikipedia.org/wiki/Artificial_intelligence), [robotics](https://en.wikipedia.org/wiki/Robotics), [animal cognition](https://en.wikipedia.org/wiki/Animal_cognition), [plant cognition](https://en.wikipedia.org/wiki/Plant_cognition) and [neurobiology](https://en.wikipedia.org/wiki/Neurobiology).

Embodiment thesis: *Many features of cognition are embodied in that they are deeply dependent upon characteristics of the physical body of an agent, such that the agents beyond-the-brain body plays a significant causal role, or a physically constitutive role, in that agent's cognitive processing.*
—RA Wilson and L Foglia, Embodied Cognition in the Stanford Encyclopedia of Philosophy

This additional embodied–semantic link accounts for advantages in processing speed for abstract emotional terms over otherwise matched control words.

Emotion knowledge is the manifestation of emotions in actions which becomes the crucial link between word use and internal state, and hence between sign and meaning.

An image schema
[Mark Johnson](https://en.wikipedia.org/wiki/Mark_Johnson_(professor)'s book 'The Body in the Mind' is a reference for the concept of an image schema.


### Biosemiotics

<http://wp.unil.ch/biosemiotics2017/files/2016/09/Biosemiotics_BoA_24.05.pdf>

<http://www.academia.edu/727564/Towards_a_Semiotic_Biology_Life_is_the_Action_of_Signs>

<https://www.biosemiotics.org/biosemiotics-introduction/>

A semiosphere is a world of meaning and communication: sounds, odours, movements, colours, electric fields, waves of any kind, chemical signals, touch etc.

Specific semiotics niches have to master a set of signs of visual, acoustic, olfactory, tactile and chemical origin in order to survive in the semiosphere.

Organic codes and Processes

Biosemiotics is dedicated to building a bridge between biology, philosophy, linguistics and communication studies.

The study of representation, meaning and sense.


### Fractals

Stephen Wolfram and the one combination of the program, which he called [Rule 30](http://www.wolframalpha.com/input/?i=rule+30) (or 00011110)

Fractal behaviour?

<http://object-e.net/tools/attractorfields-tools-gh>

<https://generativelandscapes.wordpress.com/2014/09/15/fractal-terrain-generator-example-9-3/>

<https://www.youtube.com/watch?v=_3_7sDce6ME>

<http://artseverywhere.ca/2017/07/21/fractals-part-1/>


### Methods of interest

A paradigm shift

A symbiotic system

A common language

A parametric design (adaptive, resilient, constant development)

Regenerative design

Critical design

Interactivity

Communication

Living with the ideas

Materialising the abstract


### Key terms

Consumption (resources)

Economic activity

Scaling down production

Anthropology (human behaviour)

Psychology

Haptic experience

Linguistics

Neuroscience

Sustainability

Self-sufficiency

Circular system

Life science

Systems theory

Communication

Urban rhythm

Archaeology (human activity through material culture)

Human geography

Materiality

Distribution

De-centralisation

Network

Locality

Accessibility


### Defining a direction

Recurrent themes in the past months:

- Data flow
- Biosemiotics, communication
- Living organisms
- Parametric design
- Anthropology
- Self-sufficiency

Attach personal meaning to those (design principles):

- Data flow - curiosity
- Biosemiotics, communication/translation - responsibility
- Living organisms - emotional
- Parametric/resilient design - fascination
- Anthropology - responsibility vs. my nature
- Self-sufficiency - independence/dependence

Transform themes into statements to convey a new perspective or possibility:

- Data flow is defining our realities. I find the need to discover those different realities through the different processing of information (ex. AI, media, researcher, individual). Cultures and identities created and lost in the digital translation.
- Communication within and between systems is essential in order to build more resilient futures. This exchange of knowledge is helping to sustain one another. The study of signs. Production of identities and meaning within a symbolic system.
- My roots are attached to the natural environment.
- Constant change/ cycles because inputs can change its transitory outcomes.
- Learning and understanding human behaviours is about understanding myself first.
- At the Earth, city and community level, there’s a need for dependence between nature, humans and technologies. And at the level of the individuals, there’s a need to be more independent, be able to produce on what you need.

Define research questions (asking “How might we…?”)



## Vision development plan

WHERE I AM NOW?

• What are your strength and weaknesses?
• What kinds of options and different routes do you have on your current learning path?
• What kinds of learning methods and styles are most effective for you to use? Why?
• What kinds of things and ideas motivate you? What makes you enthusiastic?
• What are my current working environment challenges and what part do I have in solving them?

My design identity is in the process of metamorphosis. I am progressively developing an understanding, using new resources, skills and opportunities, which are expanding my abilities as a designer of the future.

Until now I have had the ability to take time to make mistakes without any crucial consequences, which allowed me to discover unknown territories of interests and research.

Through the past years, I have used different learning methods, including problem-solving, critical-design thinking, and mostly speculation. Internships have also helped me understand decision-making methods when put in real contexts (time and money becoming key factors).

I have worked considerably on ways to create a narrative that will communicate ideas, however I am missing a coherent visual narrative in the process of design.

I’m motivated by the idea of communication as a key aspect of design, along with design being a political tool, able to influence every aspects of our reality.

The natural environment is something that motivates me to understand it, but also it inspires me, and helps me question human being’s role in comparison to its role. The idea that everything has meaning but it’s a matter of how perceptions. Shape is function and there’s only an infinity of it to explore.


WHERE AM I GOING?

• What are my long-term strategic goals (2-3 year span) in professional development?
• What are my short-term tactical goals (1/2 year span) in professional development?

I set myself this year’s challenge to develop a physically connection between a speculative idea and the real context of its application.

By continuing the current quantitative system of production and consumption, in society as well as the technological developments (for instance AI), data is becoming increasingly important. Data is constructing our reality. Acquiring some knowledge around data collection and analysis could lead to jobs in consultancy companies, allowing to understand current trends but also manipulate behaviours through the use of those.

I do not know where I see myself in the next three years at this point in my development, however I know that I will not want to stay in speculation, design new realities from a 3 meter long desk but instead I want to define various contexts and situations as my “desk”. I want to start actively making connections, try my own ideas, share experiences and knowledge, convince of the importance of a designer’s philosophy.  


HOW DO I GET WHERE I WANT TO GO?

Use empathy to design a powerful experience. Collect subjective insights (by embedding myself in the context- gain personal insights) with objective reasoning and analysis (using knowledge). This could be done in four stages, starting with discovery, following with immersion, connection and detachment.

My challenge is to create a strong network and relationship with people and things that can help me bring to reality a speculation.


HOW DO I KNOW THAT I HAVE ACHIEVE MY GOALS?

Kickstarter, working team, defined identity, end to frustration at a specific stage of the project, feeling of making a difference in a community (design or people).  



---------------------------------------------------------------------------------------------------------------------------------


https://diytoolkit.org/tools/

Gregory Bateson - anthropologist, linguist, semiotician

Merleau-Ponty “Phenomenology of perception”

Lucien Lévy-Bruhl

Edmund Husserl

Jesper Hoffmeyer

<http://archive.fabacademy.org/archives/2016/fablabbcn2016/students/284/finalproject.html>

<https://files.eric.ed.gov/fulltext/EJ1102484.pdf>

Eduoardo Kac - Genesis of communiaction

Carl Honoré, "In Praise of Slowness"

The Slow Media Manifesto

Johanne Seines Svendsen, 2013

Abraham Maslow, 'Peak Experience'
