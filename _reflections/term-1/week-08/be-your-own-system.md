---
title: 8 | Living with Ideas
period: 19-25 November 2018
date: 2018-11-19 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Angella Mackey and David McCallum*</span>

![]({{site.baseurl}}/PB193330.jpg)
*A data sensor*

This week was constructed by a series of workshops aiming to bridge speculation to present as a way to observe it. Each of those were using different methodologies to approach speculation and its application in the real world.

The aim is to create small disruptive interventions that can get me a direct response, answers, results, to better understand how it could work. And see those experimentations as insights; done through the stages of research, action, results analysis, design repeated continuously as a way to improve the (temporary) final outcome.  


## Materialising an abstract idea

First challenge was to design a speculation physically connected to reality.

The first method used was the materialisation of a speculation in a short amount of time - extending to approximately 20 minutes and followed by a short explanation of the process and the artefact. The exercise was to take a random object an build and understanding of the world it would live in if it was given a new purpose and actually live with this repurposed object. The idea is to experience that object to learn and see things that are new and get a new perspective of the reality through the object but also learn how an artefact can be used today to act as a catalyst for transformation.
How does it affect you when something that isn’t alive becomes alive?

For a reason or another this exercise was very difficult for me to try. I probably was looking for reasons to do the things I wanted to do and in that amount of time couldn’t find those reasons. However the task was not to create a concept but to simply create a “magic machine” that could be the bridge between the speculation and the reality.


### What if?

What I have understood from this exercise is that to create this magic machine, the idea doesn’t need to be inspired by the object but come through the object. And so I asked myself a few questions as a way to start the exercise.

What if living organisms could create, read, respond to pixels?
What if pixels were forming bio-materials?
What if living organisms could be responsive to pixels?

Following those questions, I decided to have as my *magic machine* a reading device (or data sensor), using an incense wood to read screens display and translate that language using the olfactory sense.

The kind of data I would get from reading using smell were very visually focused and not so much on the interaction itself.  If I lost that device, would I loose a memory? Would the mix of smells and the mix of pixels correspond to create information?


## What did I learn?

Knowing what I learned from it will help me create a project that will work better later on.

How does the translation between the senses make me understand my environment differently?   During the short time of use, I noticed how focused I was on digital devices and how much more aware I was of smells present around me and in the room. Smell is a powerful tool that can be overwhelming (and in my case was). Smell is a strong tool to create and bring back memories - a sense strongly attached to time.

This sudden and unusual attraction to light and screens made me curious of digital devices rather than people resulting in a change of behaviour. This *magic machine* was creating a link between users and displays only, which transformed my initial intent.
I discovered smell as a language of its own, capable to give you an object’s identity as you use it. It is communicate between humans and devices using the human sense as the tool for its actions.

The bigger points made clear during this exercise was, first, the importance to believe in the idea, second, to believe in yourself and to not be scared of failure or the ridiculous but instead push the idea. It is a matter of trust, always. The trust developed towards the technologies we are using can invalidate our own beliefs and rapidly create a sense of insecurity by losing one’s  trust and therefore self-confidence. Knowing we are relying so much on those devices, can we predict what would happen without those?


## Living with ideas

![]({{site.baseurl}}/47386358_219891565575047_3342710266939834368_n.jpg)
*A daily mindset. Etching by Pierre Emile Moulin, "Z".*

### Angella’s PHD research

Fashion film to create desires, moods, ...
“Wearing Digital Shimmers” project by Angella
Signify: The meaning of Light
ArcInTexETN

Use technology to make it into a wearable form. - Wearable Technology.
“Vega wearable light”  - discovered people are interest in the idea or wearable tech but not actually wanted to wear them. She wore her product for cycling herself and helped her design it.

PHD starting point:
How w.t. was never really translating into the real world. Can you start wear it without it being entirely developed. Search for the day to day experience of an emerging world of tech.

Look at, what she calls, a “dynamic fabric”.

Visual change according to computing input. Have w. That have the capability to act like computers. Wear the same cloth that could change her everyday life.

“Auto- ethnography”: lived through her design as a strategic part of her research, observe behaviour change, etc. Write down daily thoughts and feelings and periodically, every month look at it to find pattern or theme and start formulating your concept.
Wear in front of an online audience in order to have one audience.
“Gimmick stage” playing without giving any meaning to it - gave time to see patterns within months.

Take images from environment to wear. Why? Blending with other things in her environment.
Got hacked - not in full control. Other people playing too

Interplay between physical materials with digital patterns - used different types of fabric to affect how the chromatic worked.
Help mitigate fast fashion with this project? By wearing she realised she envied more choice for different effects… Not a finding because it is based on one experience however she can challenge that idea

Why was this project useful?
Allowed her to be much more precise, building on the parts that made sense, taking in account some live feedbacks.

Does the film have to be speculative? Balance between mundane and highly stylised. Create a concrete setting. Create a format to express everyday life. Film as a “platform”.


### Wearing the digital

What would I wear? If I could wear fabric that acted like a computer screen or that could change dynamically, I would wear a coat to share daily information with people in my back in the city or wear my mood.
How did it feel to do this? Confront myself doing this, taking picture of myself, unaware, wear something that I didn’t choose to, etc.

What was essence of what she was trying to do: how was having this affect a daily life.
Aim: How to explore an emerging technology that doesn’t exist yet?
Aim: try to create a system.

An exploration not looking for a specific goal. Goal was to commit to live with it everyday and make it work as a wearable. Break down the essence and try to find what to mimic.
The best way to use this research methodology/ technique is to use a lot of time to live with it so that it can go past the stage of gimmick and become something specifically designed for a purpose…

Failure as experiment results helping to shape your project too.


## Creating a speculative artefact

![]({{site.baseurl}}/PB213362.jpg)
*Translatability in between systems*

### *What ifying* together

The common interest in this group was the translatability in between systems: the digital, the physical and the organic. We started by asking questions and trying to define further the consequences of it if applied.

What if you could grow AI / the digital?

What would it grow as?  What would it look like? Where would it grow? What would be its nutrition? What is growth? What if it contracted instead of expanding? Might not be sthg that *grows* from bottom upwards. Would it develop a different spatial behaviour? What kind of relationship would develop with AI? Would it connect to other AI? Translatability within AI? Growth would happen in a different context? Does that context need to be physical even? The physical location might not be as relevant because AI is everywhere, creating a network with many stations and links.

What if you could harvest AI?

What tool (if even necessary) would we use to communicate? Different kind of information used for the growth would result in different form with a different communication skill/function.
What if need to find the AI, camouflaged? Create a detector to find it or find where it grows? AI not perceivable? How to detect if?


### Object’s application

Food as 1s and 0s? Food found in context/environment for growth? Or doesn’t matter? A tool for communication?
Make someone use the object created. Make an observation of behaviour change?
Scenario of moments in which they connect. Glove to medicate connection.

As we were living with our artefacts, I wanted to right down some part of the conversations we had together or with others, creating a curious narrative:
*“Oh damn I lost my finger”
“My hand is so sweaty”
“This is not working, my hands are too sweaty”
“My finger is heating up”
“It’s getting purple now”*

It would evolve on its own, and we were questioning whether it would live on its own once it’s host dies. AI as a host for space but not for nutrition. The nutrition is not physical/immaterial/… Not a parasite. I imagined it as a form of intelligence, you can get if from birth (like a communion or a ritual) and it will develop, grow with you as you grow.

Connect reality of wearing it to the digital using QR code idea - hands in front of digital screen camera to send you information or send you somewhere online? A sort of communication through form and shape. What is our mean of connection to our network?

## Living with it

This third day was mostly a day for reflection after experiencing the challenging two first days.

![]({{site.baseurl}}/PB213421.jpg)
*My status presented to you*

Exercise 1: STATUS.

What is the *status*? Basic desires are defining the *status*, and that status is defined on the given piece for paper, as the need for social significance. Or is it instead something binary that you can turn on and off whenever you feel like? Is a status defining your identity or vice versa?
I think it would be interesting to use status as a way to be significant in the eyes of the ones that do not know who you are and therefore do not value your status.

Name of the magic machine: Undefined.

What it does: I wanted to find a way to make someone who doesn’t know you to face your *status* and understand its value, no matter the type of status.

What have I learned from it and about the idea of *status*:
I have in some way failed to complete this exercise and was confronted with this situation. Why was it so difficult for me to create a basic, straightforward interpretation of my idea in fifteen minutes?  It is subject to relativity. It is a variable.


## What and How?

Connection and responsiveness to organic elements, using digital as a tool, and put in context through the human body.

![]({{site.baseurl}}/PB213412.jpg)
*Exercise 2: CURIOSITY.*


### Observations

Shape is function. The shape of growth is organically influenced, digitally defined, and felt physically. Seeing the growth of something will create a stronger connection with another thing.   
Growth and its connection to things. Or live through the perspective of a plant?

What if the natural environment/nature was replaced by humans bodies?
What if a plant was a hand?  

How do I use it? What does it do? The hand would look for interaction, need for attention, cared for and caring.
What do I learn from this?
Define abstract/broad terms by showing the other extreme. So what do I mean by nature and digital. By exploring what I don’t think those terms are, I can define what it means and be more specific.


### Conclusion

This week was a *reality check*.

Those exercises have been difficult challenge for me to take. I believe the reason for my struggle is the difficulty to try simplify complex and broad ideas into one single idea that should be physical and with no real problem solving.
I’ve learned to design all sorts of things with always a reasoning behind it, backed up with research and time to discuss and develop the idea. In those past experiences, I have been taught to always question why I did something the way I did, and how it does it act in the problem solving. I learned to design in a way that I could always justify my decisions and actions, and they were always questioned when time came to present the ideas. I think this probably was something essential as it is giving, in a way, a proof of concept (just the concept but not its embodiment). The realisation of an idea into a physical thing for application was something really done in a spatial design bachelor like I did. That is something I was clearly missing and looking for; to create a tangible form of an idea, not just as a scaled model but as something that could be used and tested. I need a proof of concept as a whole, and that can only be done if materialised in some way.
In this exercise, we were asked to do something with more intuition and spontaneity given the small amount of time and the limited amount and types of materials at the disposal.


How can suddenly your imagination be completely absent? What is making me lose the capability for inventiveness? Where has gone this intuition, this naivety of a child which is giving so much freedom to the mind to create something new. Have we seen too much innovations? Have I become numb to new ideas? Has my brain developed a certain impermeability to the idea of newness? The world in which we live is pushing us to create more ideas that will compete against each other, and with a bit of money and good network, will be able to flourish as a tangible and working idea. Therefore I find myself in this situation where I constantly question or dump ideas as I feel they are not good enough, partly because I feel it will have been influenced by the many ideas I have encountered in life or in the ongoing research of new knowledge. I need to trust my instinct and my ideas and if not good enough I need to push the idea and develop it in a direction that will make more sense. How can I train myself to do that? Should I train my brain to let go of the rational for the sake of those types of exercises? How?

Empathy.

I discover empathy as a disadvantage and I am now trying to redefine it as a tool to make people more sensible to their direct environment, but also to help understand how one’s actions or presence has an influence over the animate and inanimate elements of that environment.

So how could empathy play a role in creativity? Or how can creativity play a role for empathy? Can oneself develop empathy by activating its imaginative process?

What is empathy? I would define it as a “stimulated emotional state” relying on the ability to understand and care about another’s experience or perception. There are two types of empathy: one cognitive (imagine yourself in another’s reality) and the other affective (sense the perceived emotions of another). I would say I have a affective empathy but in some cases I have experienced a more cognitive one. How does it work? It is using mimicry as a way to perceive the world through another’s perspective, this technique activates mirror neurons helping to understand attitudes and intentions, which in turn can help prevent or solve future situations.

Storytelling can be another method used to create an empathic reaction as someone identifies with the person used in that storytelling.

But this ability can also go beyond its constructs, by addressing our personal experience with something (for instance the natural environment), and tries to understand the perspective of other(s) by questioning *why* it did what it did and *what* is the purpose of those actions.

-----------

### References

[David McCallum](www.sintheta.org)’s work is bridging the physical and digital realm through performance.

[Angella Mackey](www.angellamackey.com)’s work is oriented towards media art, wearables and technology.

[Google is working on touch screen fabric](https://www.wired.com/2015/05/google-wants-turn-everything-wearable/)

Audrey Desjardins’s [Tilting Bowl](http://www.audreydesjardins.com/research/tiltingbowl.html)

[Fibers and fabric to harvest kinetic energy](https://specialtyfabricsreview.com/2017/09/01/fibers-and-fabrics-play-a-starring-role-in-the-effort-to-harvest-kinetic-energy/)

*Arrival* (2016) - a film directed by Denis Villeneuve

[Developing Empathy](http://www.informalscience.org/sites/default/files/Empathy%20best%20practices_lit%20review.pdf)
