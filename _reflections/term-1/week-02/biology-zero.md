---
title: 2 | Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Nuria Conde and Jonathan Minchin*</span>

![]({{site.baseurl}}/BiologyZero_Gif.gif)
*From Zero*


### Reflection

One of the ﬁrst things that was discussed, was the importance of **democratising technology and knowledge**. Through open-source projects and DIY bio labs, not only we can expand the possibilities in the ﬁeld of science, but we can change the way we look at design and science and how they connect.
On one hand, it has been clearly shown that by sharing this knowledge, we can limit the costs of materials needed, and save time by using what has already been researched, experimented and shared. On the other hand, I question if the democratisation of biological knowledge could have considerable consequences. Should it be available for anyone to use in the way they like? Could anyone apply those discoveries at a bigger scale, with a greater risk of creating something with negative impacts?
But most importantly, **isn’t using those technologies maintaining or even accelerating the rhythm** at which we currently produce things? Is that the right method for reaching a more sustainable system?  

<span class="text-large">**It is a matter of scale.**</span>

![]({{site.baseurl}}/Phylogenetic tree of life species entities.jpg)

In design practice, we look at a scale to which every human being could probably relate. It is building a reality that makes visible human scaled changes (such as architecture, urbanism, products, etc.).
However in biology, we’ve seen that the changes are made at a micro-scale creating a micro-architecture to communicate between elements of the bio system. At this scale, a lot more can be explored and tested and it can try to answer questions that might not have been answered, or even asked if they had been looked at another scale.

However, is developing things just because we can a good enough reason?

We’ve discovered the world of synthetic biology - a conscious active player reading micro-biological elements and designing them to be modular - and genetic engineering - manipulating and hacking those by inserting or deleting information.
We ask what to modify, but **should we modify?** Who is “we”? How would we know the consequences? Those biological practices are playing with the only thing that keeps us alive - nature. It feels there is less possibility of coming back when manipulated from the centre of life.
Is genetic engineering **an intervention or a discovery?** Can a discovery be an intervention? Those methods are research based at a tiny scale that can have a lot of power. How can you apply your discovery in a diﬀerent ﬁeld and at a diﬀerent scale?
I understand that in order to make those discoveries, we need explore as much as possible and push the ‘limits’? This means leaving the ethics and morals to the side during this research and testing. However, I feel it is essential to come back to it or simply stick to it in the process, before application, in order to limit the risks of making irreversible damage to our society or environment.

Shouldn’t we learn instead to **moderate** our way of consuming, producing, and creating?

![]({{site.baseurl}}/WhenMatterLeadsToForm.jpg)
A systematic way of thinking of sustainability.

Mette Bak-Andersen, from the Material Design Lab (KEA) introduced us to her **material driven design** practice for sustainability. We discussed circular systems, the ever changing meaning of ‘design’, but most importantly the need to know the possibilities of materials to then create a useful and sustainable concept.  
How can we reinstate materials as they are now, in the current education system of design? The making of complex concepts has become more important than actually making them with the ‘right’ method and materials.  
**“Living materials”** is a very interesting term that I discovered during this conversa<on. The idea that this materials are sourced by a partly waste from the natural environment, and the idea that they  are adaptable in the process, allows us to understand better how it behaves and learn from it.
Through the crafting and making of those new materials, we feel and see things that makes us understand its potential, and allows us to exploit those qualities in a beneﬁcial way. Are they “augmented materials”?
The materials are creating an environment for the explorations to take place. Without context, the results of an experimentation can not be valid. The only way to make sense of results is if we understand that it is needed in or created for a **speciﬁc context**.

How can an organism be a sensor for a speciﬁc environment? Then what could be learned if the environment changed? We have learned several recipes for auxetic mediums and tested those to see what kind of microorganism would live there and how they would grow.


**“Shape is function”.**

 A crystallography shows how the shape of a structure can indicate a function.  Can we develop a speciﬁc shape to create a new function and apply at a diﬀerent scale? Can the pattern created by those micro-organisms have a purely aesthetic purposes?
 This bring some back to the practice of biomimicry and how eﬃcient it is today. Could we include micro-organism in the process of biomimicry to allow better adaptability?

**Scientific Method**
![]({{site.baseurl}}/ScientificMethodDiagram.jpg)


Can a biotic system inform current environmental, cultural, economical architectural systems?  

**Hypothesis**
Architectural typologies will be replaced by biotic systems.

What would be the consequence of this hypothesis? Cultivated buildings will be more adaptive.

**Methodology**
Could a building change of structure to change of function during a deﬁned period of time?

- Architectural typologies will be classiﬁed.
- Functional needs will be analysed based on those typologies.
- Qualities of bio-organisms for adaptation and transformation in form will be selected and analysed.
- Create samples to show there diverse qualities.
- Test behaviour within different simulated environments.
- New selection of qualities of a bio-organism.
- Organisms will be identiﬁed and applied at a human scale.
- Use the organisms' shape and behaviour as a function for a structure at a larger scale.
- Observe how its shape adapts to the users' needs.
- Specific bio-organisms for specific functions will be classified for better efficiency.
- Biotic systems are replacing the architectural typologies - a reality.

**Reflection**
How have we come to the point of creating things that are static and can’t change or adapt to the rhythm humans and society has today. We are constantly changing our patterns, lifestyle, values, … How can architecture respond to this change using some of the technologies developed today. Reinstate nature to adapt to current urban rhythm could be a possible solution? Use “nature” to learn moderation?

We have been applying the function of living organisms to man-made things for centuries and today we have reached a point where the copying of this nature is not enough or relevant enough. The actualisation of nature is needed in order to evolve towards more sustainable environmental, cultural, social and possibly political environments.


---------


### References

[http://igem.org/](http://igem.org/)

[https://learn.genetics.utah.edu](https://learn.genetics.utah.edu)

[http://bio.academany.org/](http://bio.academany.org/)

[https://diybio.org/](https://diybio.org/)

[http://www.thelabrat.com/](http://www.thelabrat.com/)

[https://www.genspace.org/](https://www.genspace.org/)

[https://www.ginkgobioworks.com](https://www.ginkgobioworks.com)

[http://www.fao.org/home/en/](http://www.fao.org/home/en/)

Video: BBC Our Secret Universe The Hidden Life of the Cell

Article: “The Evolutionary Framework for Design for Sustainability” by Ceschin, Fabrizio and Idil Gaziulusoy, 2016

Book: “Butterflies of The Soul” by Santiago Ramòn y Cajal

Book: “Appropriating Technology” by Ron Eglash

Book: “Bergey’s Manual of Determinative Bacteriology”  / Edition 9 by John G. Holt, Peter H. Sneath (Editor), Noel R. Krieg (Editor)
