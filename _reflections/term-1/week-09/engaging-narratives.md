---
title: 9 | Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-11-26 12:00:00
term: 1
published: true
---

*Introduced by [Heather Corcoran](https://www.kickstarter.com/), [Bjarke Calvin](https://www.duckling.co/), Tomas Diez and Oscar Tomico.*

![]({{site.baseurl}}/.jpg)

## Practical Narratives

Last week I briefly questioned how storytelling could be used as a method to create an empathic reaction as someone identifies with the character or situation narrated. Storytelling can be an effective way to influence change in a person’s behaviour, towards a more trustful, caring and empowering method. By listening to stories, we can develop a better understanding of the self and of the others, and eventually encourage personal growth and create a better adaptability and flexibility to events. These positive outcomes can only happen if the narrative is taking into consideration ethical concerns, to avoid doing the very opposite.

This week we have been introduced to two different online platforms with the intention to use narratives as opportunities to invite the audience to get involved in a creative process - as a collective thinking - but also to communicate and share those stories as a way to give insights and a better understanding of our present and our possible futures.


### Exercise 1
The first exercises were conducted by Heather Corcoran after taking examples on Kickstarter.

- Title: Inanimate nature / The responsive you / The inanimate animate / Inanimate intelligence
- Subtitle: Taking part in a communication between the animate nature and the inanimate reality of the environment. An experience fed by shapes. / An application giving you insights on the animate and inanimate natural elements present around you at a specific moment.
- Lead IMAGE: Specific iteration? Scenario??


### Exercise 2
Develop this first exercise into a proposal outline / storyboard.

![]({{site.baseurl}}/Duckling Story.jpeg)
	Sketch/Storyboard p.152

A platform giving you a notification or message from your inanimate and animate surrounding.
Example you will get a message on your device telling you to listen to feel the wind going through the room, think of the weather…
A kind of radar telling you how to communicate with those, sometimes unperceived, elements of the environment.
The technological screen device as the interface

Look at “stupid ideas” on a broader scale to understand how the trends are forming.

Use social platforms and make it a habit to share your process for the people.


“A creative person’s guide to thoughtful promotion”, an [article](https://thecreativeindependent.com/guides/a-creative-persons-guide-to-thoughtful-promotion/) by Kathryn Jaller.

“Tool from the business world that might help you in forming your goals is the SMART system. Based on this acronym, goals should be specific, measurable, achievable, relevant, and timebound.”

“In order to promote your work effectively, it’s important to understand how a word, image, or idea might affect someone’s emotions and stimulate their senses.”

“exploring your audience are demographics and psychographics. Demographics are facts such as a person’s location, age, family structure, income, and education. Psychographics address an audience’s attitudes, aspirations, and behaviors.”
	Create personas

“Promotion as collaboration”
	Open systems analogy - open up practice for conversations.

How to be creative today? Take a different method or approach (for instance a business model - https://diytoolkit.org/tools/ ) to trigger creativity or follow up on the idea.
Promotion can be very tactical in the way that is helps put yourself and your idea out there for reflection and criticism that will help you grow as a person and as a designer. Careful not to think the first audience you get should be the only one - need to balance the types of audience to get balanced feedbacks

![]({{site.baseurl}}/Engaging Narratives Growth Interests.jpg)

## What is my area of intervention?

We’ve been encouraged to familiarise ourselves with our area of interest using the strategies that have been given every week in this first term, which has been focusing on articulating a journey. This is helping us to define what the door looks like, to then in the second term find out what is behind it.

The areas of interest I’ve been looking into aim to bridge living things (plants, microbes, fungus) with humans and machines (what machine? Ones accelerating the pace of production and consumption) as a way to reinstate nature in the urban context (physical connection, highlighting necessity, understanding mutual needs) and create a responsive culture to the natural environment. These aims and interests reflects my strong belief that the presence of “nature” is essential in whatever futures we create for ourselves, and knowing the futures will include AI and advanced development in all sorts of technologies that might push us away from the “nature”, It is important to try develop a communication and understanding of this exchange between the different realms.

I have been looking into information flow, biosemiotics, communication, biology, embodied cognition, fractals, parametric design, anthropology, language, self-sufficiency.


I have tried to brainstorm all of my current interests  to create a sort of a network and highlight the ones that have more links to go in depth in those specific areas of interests.
The way I wanted to do this was by creating a kind of infographics in order to present ideas and their implications in a potential project in a very simple and instant way. I am very interest in learning Grasshopper to use as a tool to help me explore the different layers, groupings, scales, time scale, sensory experiences as part of this network. I am in the process of making different taxonomies to give order of my thoughts and interests using Grasshopper and parametric design as a method and tool. The variables created to represent the selected interests would change in function of the stage in which I’m in the process of the project. By getting instantly and visual representation, it makes more engaging and interactive for me and others to play with and explore.
I would like to develop this idea as a way to combine interests.
Could emotions be a branch of interests, interesting to add to the network?

The ‘Growth’ grasshopper experiment is a methodology that could then evolve into a physical experience giving new possibilities for interaction in the physical world and allow further interaction with physical elements such as organic entities, human body, buildings and materials,...  It could translate between the digital and physical and vice versa. This will second metamorphose will make it into something new that has the tools for actions to be taken from the individual’s own motivations.
Do I want knowledge or mindfulness from this possible physical interaction?
This ‘Growth’ methodology is aimed to become a tool for actions to be taken.

![]({{site.baseurl}}/Grasshopper Growth.jpg)

## What are the “guru” of those/this area?

- Gregory Bateson - anthropologist, linguist, semiotician
- Merleau-Ponty “Phenomenology of perception”
- Lucien Lévy-Bruhl
- Edmund Husserl
- Jesper Hoffmeyer

## Narrative of the development of my interests

Why am I attracted to look into the growing, the breathing, the physical, the organic, the sensing, the communication, and the complexity of simplicity of things, in design and life?

Why do I not like the static - Present and futures are created and influenced by events,  constantly affecting circumstances and possible ones. There is and has always been constant change and evolution defined by contractions and expansions of various entities. In nature these are contributing to a cycle however in our human made world it is part of a never ending line for which we are constantly redefining its curve.

In my previous studies, named interior and spatial design, I have been looking into different fields that are very closely linked to each other. To summarise what it is, I would see it as a correlation between architecture, art and interior design. Students were introduced to many examples of past and present practitioners in those fields.

Architecture has been defining the meaning of humans on this planet by giving a scale in which we were able to create our worlds and make sense of it. This human scale makes it more easy for humans to relate to in our everyday activities. It tells us about history, culture, how society works and helps us define who we are as individuals, something that humans are continuously searching for. Architecture mediating between the world and our minds.

Today, more than simply acting as a shelter, architecture is an icon, a symbol of a geographic location, and is representing communities as part of diverse cultures and society. Ultimately it is defining our identities, which are partly shaping our world.
What architecture is missing at the moment is its flexibility and adaptability to constant changes in behaviours, trends, as part of economical, social, and political situations.
The architecture, as we define and use it today, is obsolete. Those master plans of the 60s and 70s in the event of housing crisis, is a highlight of how architecture is defining *a* future and not designing for possible futures. In an ever changing environment, architecture needs to be evolving too and be more resilient to what could come in the future. But we’ve decided to keep this ancient idea of architecture as something static, a product designed to be placed in some location and then left as long as possible until its disintegration, no matter its efficiency and functionality.

I believe to make this practice relevant to the present and our possible futures, it can happen only in combination with our natural environment and its organic entities. A nature that breathes, contracts and expands, and grows.

After exploring the practice of biomimicry in my last year of bachelor, I have quickly come to understand it was missing something to me. I think I am not so much interested in the architecture itself but the communication between those elements that are part of different systems. If we can understand this relationship, we can create a new one which interwinds different things, with the purpose to complement one another.


“Massed-produced artifacts of civilization draw our senses into a dance that endlessly reiterates itself without variation.” (The Spell of The Sensuous by David Abram, p.64) and architecture is an example of those.

“The earth-born nature of materials are readily forgotten behind the abstract and the calculable form” thods materials contributing to human culture.


Define an ongoing experiment to do?


## Tools for new engaging narratives

### Kickstarter

A model of practical narratives.
Heather Corcoran introduced us to Kickstarter’s model and its approach to storytelling.


"Bring creative projects to life” focus on altruism over profit.
How is the money managed in the company?
14 million backers creating an international community.

There is a creator / backer relationship.
Why would people back a Kickstarter project?
Example Ai Weiwei trees project (packed)
Example Oculus Rift (packed)
[look online categories of projects and where most money goes]

STORYTELLING

Every Kickstarter project is a story.
Share a vision, open up your idea to the community.

Make people know exactly what it is your doing and why.
		- Distill your project’s most unique (1-3 aspects)
		- Consider scenarios that communicate those.
What audience am I trying to target can be shown directly into image or subtitle…

THE PROJECT'S REWARDS
Example copy of the thing you are making
		- A giveaway
		- An acknowledgment
		- A momento - a souvenir
		- An experience - workshop, dinner, …

Giving money like an investment - take part in the project. It is probably easier to ask money to someone as a simple way to help rather than asking for specific needs. Is it?


REFELCTIONS ON KICKSTARTER'S MODEL.

What if it wasn’t raising money but instead helping to connect or get resources to make it.
A sort of trade of skills and knowledge.
Could Kickstarter be a platform to start a debate/discussion?
Put yourself in a vulnerable position.
That could make it more real, down to earth, open to suggestions - people like to have the feeling of being helpful or simply have a purpose.

Rate of success on Kickstarter is of 40%

What if you fundraised a political idea / belief/ position or even a statement / a question instead of a project?
Has there ever been a project without a product or design as an outcome?

Is there a possible reasons for people not to use kickstarter?
agreements/contract? Method?
Not money as the needed input

How to establish the time you need to make the fundraising happen?
Is there an average time for it to happen?

Can you contact the people that are funding your project?
A follow up, ask more questions about reasons why they fund.

Project update: sharing creative process.

Building online communities through narrative and storytelling.
What are they asking for? What is needed and not needed?

### Duckling

Duckling is made for “insight media”, a next media category built on “contextual and collective human thinking rather than personal promotion”.
“Through Duckling, people create stories about their key insights, that are passed on, changed and expanded by the Duckling network. The result is ideas, insights, and inspiration that deepen our understanding of ourselves and the world.” says Bjarke Calvin, founder of the application. Duckling is making direct reference to the story of “The ugly duckling” written by Christian Andersen.

### Duckling first story
![]({{site.baseurl}}/DucklingStory.jpg)

### Documentary storytelling

What is a story and what isn’t?

Public speaking as a way to promote an idea, a project, a theory can be turned into a powerful narrative. TED Talks are an example of inspiration for this method of storytelling. A story has to focus on one point, that will stretch into a line as the story is told. It is what makes the difference between a collection of stories - for instance Instagram - and narratives.
In an age of photography, we have become visual beings. We have acquired photography as a method to capture any kind of information - from text, to colours, to shapes, to textures, etc. - which together can form and express a one shot story. A photograph can be very powerful to communicate ideas and knowledge in a “universal” language. It’s meaning can be interpreted in different ways in relation to every individual about will always be understood in way or another. A visual documentary of a process of thinking can make it accessible for anyone to participate in. However how can a visual be strong enough to go past the numb feeling to information, in a world that is occularcentric? What can we make a visual information appear as good enough to be look at? There needs to be something bigger for other people to refer to personally, which will give them a reason to care.
Another way to get people’s attention is to build a stronger message by using references to back it up. The aim for storytelling, such as in Duckling, is to give insights and to do that, the things that are worth sharing need to be found out and selected to further an idea.

Liam young, an architect of fiction working between the fiction and the documentary.
Conferences in the form of a story told for you to imagine this emerging world that he has created for us to discover. A piece of art? A documentary? An architecture? A space for reflection? A space to dream?

How to tell my story?
What if my story was told through sound (music, spoken word poetry, ...)?
What if my story was told through touch (texture, heat, materials, ...)?
What if my story was told through movement (performance, dance, breathing, ...)?



## Further reflections

Here I am again, enquiring reading materials (books, articles, podcasts, magazines) and letting them pile up in a place without reading them. The Japanese have a word for it, Tsundoku, which I came across earlier this week and found very relevant to my situation. In the process of acquiring all the given tools and knowledge these past weeks, I have been struggling to read all the documents I would like to read. Lost in my chaotic thoughts and reflections, I try to filter the information we were given to be able to come out with a more understandable version of the state of my mind. I am trying to narrate this process of organising thoughts and making sense of ideas to try to take a direction in becoming more of an expert in a specific area of interest. I have felt the need to take the time away from digital screens at several points, as I needed to find myself again without depending on those technologies but only on my intuitions and what I enjoy doing.
This time-space is essential to me. I need to discover better who I am and gain self-confidence in order to help others, and self-confidence seems to be a life-time process narrated through life experiences and reflections. The past three months have been very helpful in constructing a better understanding of the present and ways in which we can construct new realities that could have the force to connect others together.
“Each one of us are a construction.” says Juhani Pallasmaa. We constantly construct ourselves through reading, imagination, spaces and entities as a way to construct our identities.

Sharing realities.

Last weekend, I had a long conversation with someone that shares a very close values and understandings to mine. I was explaining that whenever I meet someone that doesn’t share the same beliefs as me concerning our purpose and power as individuals in society, I felt the need to *convince* those people. I feel so strongly about certain things that I can not always justify, and use those intuitions and instinct to guide my actions. This *intuitions* have been built up from my childhood, through cultural, emotional, economical, social experiences. If I can understand that, I should understand that there’s clearly a problem by conversing with this aim in mind. And this is because I am missing the listening part of the conversation. I make the mistake to not record or to understand their motivations for their own believes, one’s own instinct.
From this conversation, I understand I have this strong desire to teach and tell my story, my instincts and what have formed those. For this to happen, there needs to be a sharing of different ways of thinking, within those complex subjects of debate, in order to try understand the reasons for those differences. I want to converse with one’s reality.

My reality is not the reality of the others.
There isn’t one reality but many realities constructing today’s world.

Narratives are going to be essential to communicate between those different perceptions of the present, and could be a way to link those as a way to find a common solution.


*Note: During Hangout video call, questions were sent via hangout group message. Much more questions, no attentions drawn to the ones asking.
More freedom and willingness to ask.*


--------------------------------------------------------------------------------------------------------------------------------

## References

[West world]
series and film

[Doppler headphones]
what would be your trust like if the AI took the voice of your mother?

[Life 3.0]

[Dave Hakkens - precious plastic, phone bloc]

[Patreon.com]

[MIT Tech reviews on trends to come]

[TED Talks: The Official TED Guide to Public Speaking, a book by Chris Anderson]

[Unsplash picture archive]

[https://www.bbc.com/news/world-44981013]

[http://www.heathermcorcoran.com/]

[https://www.iriss.org.uk/resources/insights/role-personal-storytelling-practice]

[https://www.youtube.com/watch?v=rE_c0hmx9Fg]
