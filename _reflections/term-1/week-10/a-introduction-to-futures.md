---
title: 10 | An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-03 12:00:00
term: 1
published: true
---

*Introduced by Elisabet Roselló Román*

![]({{site.baseurl}}/.jpg)

>"Everyone takes the limits of his own vision for the limits of the world." - Arthur Schopenhauer

## 'The Future'

### What is the Future?

The future is a set of probabilities and possibilities. The future is determined by choices. The future is being made as we go along.

As previously discussed in the reflections of <a href="https://mdef.gitlab.io/julia.danae/reflections/engaging-narratives/">Engaging Narratives</a> , the agreed, common reality is experienced only through different perceptions of the same reality - as realities. In the same way, could the most probable and controlled events define our future and every other small, unpredictable events define various other futures? Could the future be understood as a form of present and the form of realities be constructing the present? Maurice Marceau-Ponty in his book of "Phenomenology of Perception" talks about the primacy of perceptions  and how those different perceptions construct those realities. In this way, the idea of a unique common future constantly changes depending on economical, social, individuals, environmental, cultural, emotional and sensing realities.

Outcomes in the future can be influenced by individual and collective actions triggered by different perceptions and understanding of our world.

### How do we understand time and history?

The idea of the West shows only one way to understand progress. Time as a continuous measurement, represented in a timeline. One's perception of time might be different from the perception of time of another. One's idea of a colour might not be the same as someone else's idea of the same colour. Colours haven't got set boundaries. Another example is music, a physical phenomenon defined by waves which create different sounds. Those sounds won't appear the same to every person just like a colour. In the same way, time can be understood in different ways depending on who and where you are. It can be seen as linear and obsolete in one, and circular and regenerative in the other. In the end, the future is defined by the circumstances in which you find yourself and one's location on the planet is what defines your ideas of a future. Those ideas are defined by experiences of the present and the past.



![]({{site.baseurl}}/SixEpochsOfRevolution.jpg)

Six Epochs of Revolution, diagram from *"The Singularity Is Near"* by Ray Kurzweil



As discussed before in the reflections of <a href="https://mdef.gitlab.io/julia.danae/reflections/biology-zero/">Biology Zero</a> , all is 'a matter of scale'. Scale will give us a different perception of what is the reality. There can be realities at different scales and sizes. In cinema, and more specifically in fiction, the idea of scale is constantly being played with and challenged to create dystopian futures.

The history of cinema, in the last century, has been shaped partly by new technologies and the expansion of media culture within various contexts. The access to those have given this industry a much more impactful position, influencing the vision of our future(s). The film industry today is expressing concerns towards our status quo under many themes.  The apocalyptic and post-apocalyptic visions were explored in film productions going twenty to thirty years back, such as the early Mad Max (1979,1981,2015), Twelve Monkey (1995), 1984, 1984), and continues today with many more productions such as 2012 (2009), The Road (2009), Children of Men (2006), World War Z (2013), Gravity (2013), Elysium (2013), and again Mad Max (2015). These films seem to promote a no future vision that people find appealing. Why? I guess it is easier to picture the end of the world rather than the end of capitalism. It is giving enough distraction to put our concerns in stand by mode, and as long as we can keep watching things within those visons, we can keep that stand by mode on for an indefinite period of time, so why not continue?

What are the consequences of those fictions on our behaviour?

We act as if we've been already defeated by our own actions and behaviours, we are giving up our power to imagine, explore and create our own visions to the bigger power of those industries. This lack of collective agenda and common social motivation strongly ties with the idea of own and only future. There is a need to change the image we have made of the future, and create ourselves ideas of what could be our futures.



### Pre-modern or non-Western ancient futures

The future as seen by people of a pre-modern, non-Western period was at the time defined by consequences of natural phenomena. One example can be taken from the longest river on Earth, the Nile river, providing the ancient Egyptians with fertile lands every time the river would flood. At the time their lives would highly depend on this natural phenomenon to grow crops, feed the population and create a trade market. Every year, the flooding would be measured to get the prospect of the success of crops.
Today, the flooding of the Nile river would mean a catastrophe for the modern cities built in that area. The Nile river is no longer used for wheat, flax and papyrus production but for housing and cities. Since the 60s it is the Aswan Dam that has been keeping this event from happening.

We have stopped listening to our natural environment and have decided to follow the economic, political and social phenomena as a way to guide our futures.



### Where do I position myself in this future?

Am I optimist about the future?

I have recently watched "Tomorrow" (2016), a documentary introducing our near future with ideas and inspirations for methods to find alternative ways of living. This film is giving use awareness and the tools to react actively to this knowledge. It is giving us the the why and the HOW under five different suggestions for change: [cultivate and learn about permaculture or agroecology](http://www.ecoledepermaculture.org/formations-permaculture-2015.html), [install renewable resources at home](http://www.colibris-lemouvement.org/agir/guide-tnt/devenir-soi-meme-producteur-delectricite), check who owns the companies you buy from and their social and environmental policy, change to a more ethical and responsible bank, and learn or re-learn the waste sorting.

This is one rare examples of recent film production that can make a change in the way people make decisions in life, but can it be seen by the same audience that is used to watch dystopian fictions as a distraction? Could we create documentaries with the communication mode of a fiction film?  Should those current movements be translated into new tendencies to be discovered by a new kind of audience? The narratives need to change, we need to move away from paralysing futures towards promising futures.



*“Advanced capitalism and its biogenetic technologies engender a perverse form of the posthuman. At its core there is a  radical disruption of the human-animal interaction, but all living species are caught in the spinning machine of the global economy.”* - The Posthuman, by Rosi Braidotti, 2013



### Narrative

![]({{site.baseurl}}/FuturearExercise.jpg)

Is helping to share this reality or belief, making it more probable by actively working on it.

Narrative as all we’ve got to express possible futures?

Structure or ‘mega narratives’, images of futures, Hypes and ads, modulators of desire, scientific knowledge, hyperstition

*“Narratives able to effectuate their own reality through the working of feedback loops, generating new socio-political attractors”* - Escape Velocities (2013)

Create patterns of how futures work? A future as a trend? Or trends as ways to point out various futures.



### Experiential Futures and Speculative Design

From circles to cones of futures, and a cone as a taxonomy of futures.

‘Future studies’ (or futurology, or futurism in the 20th century artistic movement)

Interest in materialising the interest of futures to help people imagine the world that surrounds that object.
Creating an experience to help communicate those scenarios?



### A paradigm shift

Away from human centred focus towards a system focused one.

Butterfly effect, Mega trends (economic, cultural, political powers), Social media and big corporations, Feminism (plurality of social and cultural movement), Post-truth Era.

Social and political trust schemes are in a process of tremendous disruption, accelerating the modelling of information (User, Human-centrism, Transhumanism).

New intimacy and privacy structures. The economy of data and our ocularcentric culture on social networks is transforming and negotiating the sense of intimacy and personal privacy.



------



https://additivism.org/about

("The Singularity Is Near")[http://www.kurzweilai.net/images/SingularityisNear_Chapter1.pdf] by Ray Kurzweil

"The Age of Intelligent Machines" by Ray Kurzweil

https://www.britannica.com/art/history-of-the-motion-picture

https://www.demain-lefilm.com/en/solutions

"A Sound of Thunder" by Ray Bradbury, 1952

‘Laws’ of Arthur C. Clarke
‘Laws’ of Jim Dator

Key assumptions of Wendell Bell, 1997

‘Phenomenology of Perception’ by Maurice Merleau-Ponty, 1945

Thomas More, 1516

'Cyborg manifesto', essay by Donna Haraway, 1984

Jason Hopkins, digital sculpture series on post-humanism, 2011

Tulio Crali, Cityscape, 1939

1984, a book by George Orwell, 1984

[Solaris, a film by Stanislaw lem, 1961][La planète sauvage (1973) animated tale by René Laloux]

https://www.behance.net/abhominal

'The Wood Beyond The World' by William Morris

El Hotel Electrico, 1908

https://medium.com/architizer/a-new-golden-age-of-architecture-825fb7ed652c

Franco Berardi

Bruce Sterling

'Escape Velocities', Alex Williams, 2013

'Speculative Everything', Dunne and Raby, 2013

Stuart Candy from Situation Lab, based in California

Beamer Bees, Superflux, 2008

Ramia Mazé

Sophia, first robot citizen in Saudi Arabia by David Hanson, 2017



Click [here](http://paleofuture.com/) to read more about those movement and find more references.
