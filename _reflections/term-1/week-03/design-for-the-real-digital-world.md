---
title: 3 | Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Ingi Freyr and assisted by Francesco Zonca*</span>

![]({{site.baseurl}}/PA193204.jpg)
*Studio 201*


*What is our studio going to be used for?*
* Air flow - temperature problem
* Discussion and presentation areas - circular position of chairs
* Relaxing corner for more private discussion
* Plants
* Common shelves
* Coffee Lab

*Collection of scrap materials*

![]({{site.baseurl}}/PA193208.jpg)
The week started with the collection of various type and size of scrap and waste materials on the streets of Poblenou and Sant Antoni. Those materials have been collected with the aim to rearrange our working space in the room 201 and to make it more adaptable to our needs and tasks in the future.
How to decide what material will be good to use or not? The quality, size, material type, … all influence the selection process.

*Needs in the space*

![]({{site.baseurl}}/201TablesResearch.jpg)

Table arrangement:
Need for everyone to see the screen and be in a position that ease participation and interaction with others. Is there a need for one single table for 27 people to group or could it be the positioning of group tables that make it feel as one?
4 modules for 7 people each, need for smaller spaces to be created, maybe to separate sound or feel more intimate.
Different levels for different needs or positioning for work: standing, sitting at a table, sitting lower in a informal way? Have steps to seat one or to put a laptop on?
Personal space needed to seat and work is about 1100mm x 600mm. Need enough space for everyone to seat on one side of the room.
The other half good be used for a more individual work space or relaxing area.
Make use of the tables we already have? “Noodle” concept or plug-in pieces to table to change the shape of tables and space.


### Coffee Counter

![]({{site.baseurl}}/IMG_4113.jpg)
*NOMAD coffee*

Observation: majority of people in the class drink coffee, we have come to the habit to buy coffee at the machine downstairs in IAAC or at NOMAD coffee shop close to IAAC.
The bins are filled with those disposable cups.

Why not create the space to make coffee in the room, recycling some glass jars as cups, buying coffee from NOMAD, and recycle the coffee grounds for oyster or shiitake mushroom cultivation.

![]({{site.baseurl}}/PA193160.jpg)

*What we need:*
- Kettle and water
- Aeropress or drip, glass jars for cups
- [Ground coffee for mushrooms](https://grocycle.com/growing-mushrooms-in-coffee-grounds/)
- Plants for tea - mint, sage, citronella
- Storage for coffee


### Process

![]({{site.baseurl}}/TeamInThree.jpg)
![]({{site.baseurl}}/201CoffeeLab.jpg)

Selection of materials has been done - we used plain wood boards and designed the size and shape of the table according to those.
Ergonomic shape of the furniture to be more engaging and vary its use in different parts.
Plug-in system made to be able to dismantle the shelves and replace them with shelve of a different shape for different use or simply if the shelve is not needed it can be removed.

*Changes from first proposal:*
Bottom shelf: higher, slides horizontally, ironing table structure, recycled.
Mushroom box: created a lid (instead of using shelves), recycled a wine box, organised space for Arduino to control humidity and temperature.
Vertical walls: stabilised and strengthened
Shelves as plug-ins: stacked or slid in horizontally - saving material.

![]({{site.baseurl}}/PA193183.jpg)
![]({{site.baseurl}}/PA193179.jpg)
![]({{site.baseurl}}/PA193180.jpg)
![]({{site.baseurl}}/PA193186.jpg)
![]({{site.baseurl}}/PA193172.jpg)

### Reflection

What have I learned?
I familiarised with the workshop space in IAAC but most importantly I discovered how to prepare Rhino files for CNC milling, with RhinoCAM to create the Gcode for the machine to use and then set up the axes and speed on the axes before starting the cutting.

Modularity of furniture in the room is important.
How to efficiently select materials? We have collected plenty of materials from the streets with the aim to recycle them in our designs, but in the end we’ve used a lot of new pieces of materials too from the fab lab. Either we didn’t take enough in consideration the materials we had when we designed our ideas or the material we selected isn’t good enough for use in the production of those designs.

How to efficiently use the waste material in a design?
We ended up throwing a lot of materials, we had collected, back in the streets. Why is that? Did we have to much materials?
We also used a lot of wood, why? Because that’s what we found the most? For aesthetic reasons? For ethical reasons? For manufacturing reasons?

![]({{site.baseurl}}/REHOGARrReferences.jpg)
