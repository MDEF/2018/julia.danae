---
title: Material Driven Design
period: 07-21 January 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Mette Bak-Andersen (KEA Material Design Lab) and Thomas Duggan*</span>

![]({{site.baseurl}}/MaterialDocumentation8.jpg)

>*"Not everything that counts can be counted and not everything that can be counted counts."*
William Bruce Cameron

##Sustainability and circular economy

The first day starting this seminar, Mette introduced us to the idea of material driven design as a method to bring back materials into the design process, with sustainability as the argument and motivation.

What is my relation to sustainability? Sustainability evokes to me the idea of long term, circular system, a life cycle, and durability. In order to be all of this, a design needs to be adapted to the availability of our resources in our natural environment.

Sustainability is also closely linked to a way of living consisting of all sorts of choices, whether small, such as an online click to buy something, or a type of choice that we might think requires more thinking before taking a decision. All of these actions are representing the way an individual approaches this notion.

**Design responsibility**
The moment we engage somehow with this environment, we alter its nature and create a new world.

Sustainable actions or object need to be equally viable, equitable and bearable. These three things are the links between people, our planet, and profit in a balanced way, essential to sustain itself.

**Consensus on teaching design**
A formal concept focus as a way to have design education. What is the right way to teach design today? Is there a right way? Have we found it yet?

**Design thinking**
We usually define the process of design in five steps: Empathize, define, ideate, prototype and test. In those, only two are physical matter related steps. Is this enough? Is this the right order to start designing?

**Executed design**
A design method used in the beginning of the 20th century, using real materials from the beginning. A design by doing rather than design by thinking attitude, used in the Bauhaus curriculum in the 1919 until 1931. Thinking this way meant design had control over the material and product.

**Tacit knowledge**
Craft has tacit knowledge that can not be understood by the people who haven't experienced or used craft themselves. Craft was then seen as technical and that meant knowledge could be thought in a systematic way through design thinking.

**Digitalisation**
The quality of the render defined knowledge on materials. The rendering using materials on a computer entirely redefines the relationship we have and the perception of it in everyday life.

**Maker movement today**
Makers are using the same design process which is not allowing material innovation within this important movement. There needs to be a high level of knowledge of technology to be able to adapt machines to new processes and materials.
Use technology and art to link and move towards natural science for design practice.
Technology as the tool to use once knowing the raw material.
FabLabs is probably dictating the way I think of a material or approach those. The machines available are moulding my thoughts.

**Subjective relation to material**
Sensations, touch, sound of interacting with a material cannot always be explained or measured.
Could we design for sound, design for touch instead?
When we look at a material we explore the subjective (smell, feeling, do I like it? do others like it?) and the objective (measure weight, size, resistance, can it melt?).

**Material dialogue in craft**
Knowing of vs. Knowing how
Formal vs. Tacit
Interactional expertise vs. Contributory expertise
Good at explaining what to do vs. knowing how to do.

>How to keep the dialogue going after creating the product with this material?


**Embodied cognition**
After reading all sorts of research theories and philosophies on embodied cognition in the first semester, I have rediscovered the term through the wood carving workshop in Valldaura. Shifting between online and offline cognition, I have experienced this change between conscious decision making controlled by my mind to an unconscious (automatized) behaving, directed by the body, letting go the mind. This offline cognition is reached at a point of expertise and once reached, the making goes faster. A form of meditative experience driven by the body interaction with a material.

## Wood carving in Valldaura

The making of the poon was a way to communicate between the human body and the environment. The craft is creating a feedback loop which is maintaining the energy needed to keep experimenting and creating. Those feedbacks are direct, rapid and in the end quite addictive.

This form of interaction with the material leads to serendipity. Maybe I will find out that my material has a crack in some place, which cannot fulfil what I had in mind but then this new discovery can lead me to a completely different idea or use.

Could I find the mindset found in this process in the digital format? Understanding what the digital means today might completely change in a few years line.

After all this conversation which considered craftmanship essential in our future and design philosophy, I ask myself this question:

>How to answer the needs of seven billion people rapidly enough to follow the pace of our digital and technological development and consumption using slow crafting methods?

Could we imagine further, scaled up application of those products or material design?



## Biomass
### Brewer spent grains (BSG)
------

![]({{site.baseurl}}/MaterialDocumentation6.jpg)
![]({{site.baseurl}}/MaterialDocumentation7.jpg)

"Craft brewers use more than 20 litres of water and almost a third of a pound of grain to make one litre of beer and up to 85 per cent of the spent wheat or barley gets carted off to the landfill.
The disposal options currently available to them are either expensive, inconsistent or environmentally insensitive
A way to pasteurize the spent grain with hot water, sterilizing it so no bacteria could grow. Next he injected the sterilized substrate with mycelium, the “roots” of mushrooms. The mycelium-inoculated grain produced what Villeneuve calls “a dishearteningly small amount of mushrooms”—about 500 grams of oyster and lion’s mane. When he analysed the substrate after inoculation, he found that its protein content had been boosted by the mycelium, which breaks down complex fibres in the spent grain.""

[Growing mushroom from beers waste](https://www.macleans.ca/education/college/olds-college-enviro-prodigy-is-growing-mushrooms-from-beer-waste/)

[Spent grain mushroom growing](http://plantchicago.org/2017/01/24/spent-grains-mushroom-growing/)

[Grow oyster mushrooms with beer and coffee waste](https://munchies.vice.com/en_us/article/4x5mdg/how-to-grow-oyster-mushrooms-with-beer-and-coffee-waste)

[The use of spent grains in the cultivation of
some fungal isolates](http://article.sciencepublishinggroup.com/pdf/10.11648.j.ijnfs.20130201.12.pdf)


### Birra08
----
After getting in contact with a few micro-breweries based in Barcelona, I was able to exchange a few more emails with one called Birra08 and arrange to meet a few days later. On Wednesday 6th of February I went with Ollie, Gabor, and Ryota, also interested in exploring this biomass, and were welcomed in their brewery in Sant Martí. We were given an explanation of the production process and were given further information on the waste resulting from their weekly production of beers.

Once a week on Tuesdays, they start the process for beer making which can take up to a week depending on the beer.
Behind the scene:

Every week, 300 kg of spent grain (which is used malt), or 15 tonnes a year is obtained in the process in this micro -brewery.

It takes so much time to dry the malts more than we expected.
We might need a better grinder to power the malts, as it takes much time to do so.
Extracted yeast as binder.
The oven we have at IAAC is relatively weak, therefore we may need to test the same process in stronger oven.
We could try in different shape like round, twist, etc…  
Boiling the yeast to extract from the liquid didn't work. The yeast boiled out together with the liquid.

Out of curiosity I asked how much water is being used in this process of production and I have been that the water they need to use for the amount of litres of beers they want to get needs to be at least three times bigger. They use about 3700L of water, which corresponds to about 1200L of beer produced per tank. Approximately, 4500L of beers is produced weekly at Birra08.


### Brewer's yeast
------

![]({{site.baseurl}}/MaterialDocumentation.jpg)

Brewer’s yeast features chromium as well as other essential vitamins and minerals, some of which contain high levels that help boost your recommended daily intake. These nutrients include the vitamins riboflavin (90%), thiamin (80%), niacin (50%), vitamin B-6 (40%) and folate (15%) and the minerals selenium (90%), copper (50%), [potassium](https://www.swansonvitamins.com/potassium) (18%), zinc (10%) and [magnesium](https://www.swansonvitamins.com/magnesium) (8%).

https://www.swansonvitamins.com/blog/jenna/brewers-yeast

The yeast comes from a single-cell fungus known as *Saccharomyces cervisiae*.

At Birra08, the yeast obtained from the making of the beer, is often reused for other beer that would need some extra, and if not the rest is discarded as waste. Which means they would be able to constantly provide us with at least a few litres of yeast at any point in time.



## Experiments and observations
------


![]({{site.baseurl}}/MaterialDocumentation2.jpg)
![]({{site.baseurl}}/MaterialDocumentation3.jpg)

Spent grain_

1 day old from production.
20gr as it is, equals to 6gr once dried in the oven for 5 hours at 100 Celsius degrees.

Brewery's yeast_

Few hours since taken from the tank.
52.3gr as it is (liquid), equals to 14gr once boiled to reduce for 15 min at maximum heat. It becomes a sticky thick liquid (toffee like, creamy).
I then tried to put it in the oven for 3 hours and obtained a dark brown crispy texture (sugar like once broken), still a bit sticky, with a weight of 5gr.

![]({{site.baseurl}}/MaterialDocumentation4.jpg)
![]({{site.baseurl}}/MaterialDocumentation5.jpg)

## Reflection

This material exploration has helped me better understand the kind of person I am and how I decide to work when confronted to various elements in the process. I think I have been quite stubborn with the ways of using the material and got stuck with and idea of what I wanted it to become instead of listening to the materials I was using. I was trying to make it something it wasn't and that kept me from discovering further qualities. The selection of tools I used were also defining the kind of material I could create. In that sense, if I think back about the wood carving we did at the beginning, the tools we were suggested were all made out of metal, and we were only carving in one specific type of wood. This combination of the two materials could create a certain kind of outcome which was the aesthetic created and seen in all of the spoons. In those results we could also see the differences that were defined by the movement and the unique interactions happening between the body and the objects. This observation is definitely informing my project in the sense that these interactions between body, material and tools are influenced by the methods or tools available but also the environment in which it is happening. This relationship is also creating different tensions in the process of making, mental and physical which challenge constructed perceptions.
The notion of time has played an important role along this exploration. There is no one speed but several ones, seen through the time to explore, the time every material needs but also the time I am giving to those elements. I ended up realising I was going to be short in time to get anywhere that was going to be valuable, so that it could be used for something else. I needed more time to transform those raw materials into something.

What I can take from this experience and inform my master project development is the idea that I shouldn't try one thing but several experiments at the same time and let it evolve, grow to see what is going to take roots into something that makes more sense to work with. Once I find the right experiment, I can test further more the equilibrium between those entities present in the test.

Further more, I have observed along the process and also at the presentation that every materials explored in the class could sort of complement or help each other in some way. We were progressively discovering various properties in various materials that are missing and maybe needed in others to help create something with more qualities. What could have helped for all of us would have been some short group presentation along the process all together to inform each other with what each of us have learned that could give ideas to others. This would have created a small network of information within the class that could have given more times for further experimentation that work. I have definitely got stuck with an idea in my mind and did not give up easily or realise I should try something else.
Different materials offered different qualities, some more than others, which makes it more easy or harder for different materials depending on the context in which it is happening and the time scale it is working in.

To finish, I think this exploration confirms the idea that a project needs to be made and not only thought, because it is once I started working with it that I got aware of the challenges and the opportunities I could take in the process. The process of making offers an insight on the directions available and what can work and not work. It is a physical exercise that triggers mental exercise and not the other way round, for it to work.

------------------
REFERENCES

The Human Epoch by Alicia Vikander

Circular Economy (visual) by Ellen Macarthur

Design for Environmental Sustainability by Carlo Vezzoli

[Bio-based materials](https://el-recetario.net/receta/bio_based-materials/)

[Bryan Lawson on design thinking](https://www.elsevier.com/books/how-designers-think/lawson/978-0-7506-0268-6)

Bologna process

[Tim Ingle on Fairtrade Gold Design Awards](https://www.ingleandrhode.co.uk/about-us/blog/announcing-the-winners-of-the-fairtrade-gold-design-awards/)
