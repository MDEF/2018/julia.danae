## Step by step guide


### Backup existing content


This site is using following Jekyll plugins:

- [jekyll_figure](https://github.com/lmullen/jekyll_figure)
- [jekyll-postfiles](https://github.com/nhoizey/jekyll-postfiles)
- [jekyll-feed](https://github.com/jekyll/jekyll-feed)



## Tips and tricks

### Organising your content

When writting markdown try to stick to using only level 2 and level 3 headers to organise your content.

- ``## Header Level 2``
- ``### Header Level 3``

It is assumed that the header level 1 is reserved for the page/post title.

# References

## Git

Definition from [Wikipedia](https://en.wikipedia.org/wiki/Git):

> *Git (/ɡɪt/) is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people. It is primarily used for source-code management in software development, but it can be used to keep track of changes in any set of files. As a distributed revision-control system, it is aimed at speed, data integrity, and support for distributed, non-linear workflows.*

### Learning resources

- [Git homepage](https://git-scm.com/)
- [Git commit best practices](https://medium.com/@nawarpianist/git-commit-best-practices-dab8d722de99)

### Popular git hosts

- [GitLab](https://gitlab.com)
- [GitHub](https://github.com/)
- [BitBucket](https://bitbucket.org/)



## Markdown


Markdown summary from [Wikipedia](https://en.wikipedia.org/wiki/Markdown):

> *Markdown is a lightweight markup language with plain text formatting syntax. It is designed so that it can be converted to HTML and many other formats using a tool by the same name. Markdown is often used to format readme files, for writing messages in online discussion forums, and to create rich text using a plain text editor. As the initial description of Markdown contained ambiguities and unanswered questions, many implementations and extensions of Markdown appeared over the years to answer these issues.*



John Gruber who initially developed Markdown in 2004 [describes its philosophy](https://daringfireball.net/projects/markdown/syntax#philosophy) as:


> *Markdown is intended to be as easy-to-read and easy-to-write as is feasible.*
>
> *Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it’s been marked up with tags or formatting instructions. While Markdown’s syntax has been influenced by several existing text-to-HTML filters […] the single biggest source of inspiration for Markdown’s syntax is the format of plain text email.*
>
> *To this end, Markdown’s syntax is comprised entirely of punctuation characters, which punctuation characters have been carefully chosen so as to look like what they mean. E.g., asterisks around a word actually look like *emphasis*. Markdown lists look like, well, lists. Even blockquotes look like quoted passages of text, assuming you’ve ever used email.*


### Learning resources

- [Mastering Markdown](https://masteringmarkdown.com/) - quick 34 minute mini course
- [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) - handy markdown reference

### Markdown editors

- [MacDown](https://macdown.uranusjr.com/) [Mac] - live preview of markdown
- [MarkdownPad](http://markdownpad.com/) [Windows] - live preview of markdown
- [Typora](https://typora.io/) [Mac/Windows] - minimalist writing
- [iA Writer](https://ia.net/writer) [Mac - paid] - minimalist writing



## Web Development

### Learning resources

- [HTML & CSS is hard](https://internetingishard.com/html-and-css/) - a friendly web development tutorial for complete beginners
- [Command Line Power User](https://commandlinepoweruser.com/) - video series for web developers on learning a modern command line workflow


### Software

- [Atom](http://atom.io) - code editor from GitHub, has easy-to-use Git interface
- [Visual Studio Code](https://code.visualstudio.com/) - code editor from Microsoft, popular with web developers
- [Sublime Text](https://www.sublimetext.com/) - super fast code editor
- [Hyper](https://hyper.is/) - extensible and fully customisable terminal

### Fonts
- [Google Fonts](https://fonts.google.com/) - free and easiest to get going quikckly
- [FontSquirrel](https://www.fontsquirrel.com/) - a long running source for free web fonts
- [Adobe Typekit](https://fonts.adobe.com/typekit) [PAID] - great source for premium fonts, included in Adobe Cloud subscription
- [FontPair](https://fontpair.co/) - helps you find nice Google Font pairings


## Other
- [List of popular static site generators](https://www.staticgen.com/)
- [A list of Jekyll plugins](https://github.com/planetjekyll/awesome-jekyll-plugins)
