---
title: 9 | Moulding and Casting
period: 20 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/outcome-dark-composition.JPG)

For this week's assignment I teamed up with Veronica to make a composition of several geometrical three dimensional shapes with the idea to combine some of them together and contrast with different materials. We were inspired by those animated composition of textured objects, the kind of visuals that are very satisfying to see in digital but are too surreal to produce in real. After Rhino, we wanted to try to create this composition on C4D with texture and behaviour of each element to then compare with our casting result and see how close we could get to this digital representation.

## Composition
For this, we decided on a few basic shapes and designed them on Rhino. From the shape we designed the mould directly to 3d print to save time, but also because the some of the shapes would require more than three axes to be done substractively. To not make a mistake in the different steps we rapidly sketched them.

![]({{site.baseurl}}/sketches-composition.jpg)

![]({{site.baseurl}}/Rhino-composition-moulds.png)

![]({{site.baseurl}}/composition-C4D.png)

## Design
For the first step, we drew the mould of the mould for the casting of the shape. We could have done directly the mould of the shape in Rhino but it would not be as smooth.

![]({{site.baseurl}}/SphereRhinoScreenshot.JPG)

![]({{site.baseurl}}/TorusRhinoScreenshot.JPG)

![]({{site.baseurl}}/RhinoSphereDetails.JPG)

## 3D print
Second step was to 3D print those. We used three of the 3D printers twice. The settings that we changed on the machine are the following:
- Line 2mm
- Wall line count 3
- Top layers 8
- Bottom layers 4
- 25% infill
- 60 mm/s print speed
- 210 degrees
- 50 degrees bed
- uncheck alternate extra wall

![]({{site.baseurl}}/3dprint-moulding-casting.jpg)

We wanted to get the lines as fine as possible to not see it in the final results, but it would have taken around 24 hours to print with 1mm instead of about 7 hours with usual 2mm. So we went for the usual setting to save time.

![]({{site.baseurl}}/flatten-surface-3D-print2.jpg)

One problem we encountered when starting to print, was that it was not printing on one side of the surface as it was too low for the tip to reach the surface. I asked Mikel who showed how to even out the height of the surface to make it print at the right height everywhere, simply by turning the vertical metal bars behind it.

![]({{site.baseurl}}/3dprint-mould-start.jpg)

![]({{site.baseurl}}/3Dprint-layers.jpg)

![]({{site.baseurl}}/3Dprint-time-temp.jpg)


## Moulding and casting

### Step 1
Third step was to cast the mould we 3D printed with silicone. We chose the [Formsil 25](https://formx.eu/molding--casting/tin-silicones/form-sil-series/formsil-25-silicone---10kg-set.php) silicone to create the moulds for the final cast, using the 3D printed moulds.
The mix requires part A and part B, but needs to be measured in relation to the volume of each mould we have 3D printed.

![]({{site.baseurl}}/measure-volume-mould0.jpg)

![]({{site.baseurl}}/measure-volue-mould1.jpg)

In order to make sure of the process of making and timing, we checked the information sheet. For each shape we calculated the volume and added an extra 10% to make sure it is enough mix. We had to do a mix Part and A and Part B with a ratio of 100:5.

Shape	- Exact ml - +10% ml

Sphere - 61.6	- 67.76
Torus - 25.1 - 27.61
Sphere - 127.0 - 139.7
Chocolate - 81.0 - 89.1
Pyramid - 128.0 - 40.8
Squiggle - 85.4 - 93.94

![]({{site.baseurl}}/Ratio-mix-partAB.jpg)

![]({{site.baseurl}}/Formsil25-mix-partAB.jpg)

![]({{site.baseurl}}/measure-partAB-mix.jpg)

Then we sprayed all of the moulds, taking a distance of 30cm as advised, to ease the release after the casting has set.
![]({{site.baseurl}}/composition-prep-spray.jpg)

One mistake we've made was that we forgot two of the six geometrical shapes are made out of two parts mould. Which means when we calculated the ration for each shape we forgot that for the sphere and torus it had to be times two. We didn't have enough o do all of them at once and had to repeat the mix two more times in smaller quantities to fill what was missing in the last moulds.

We poured slowly with small quantity at a time to avoid bubbles getting stuck into the liquid.

![]({{site.baseurl}}/part1-casting-sphere0.jpg)

![]({{site.baseurl}}/part1-casting-sphere.jpg)

![]({{site.baseurl}}/part1-pour-squiggle.jpg)

![]({{site.baseurl}}/step-1-results.jpg)


### Step 2
Next step was to cast into the silicone mould we've made, to obtain the final geometrical objects. We left the silicon cast overnight and they all turned out very well except for one which had some bubbles, we then lined them up with the material that we wanted to cast with.

![]({{site.baseurl}}/step-2-materials.jpg)

![]({{site.baseurl}}/step-2-cone-foam.jpg)

![]({{site.baseurl}}/step-2-process-squiggle.jpg)

![]({{site.baseurl}}/step-2-done-waiting.jpg)

We also had a second problem with the sphere that couldn't be unmoulded so we decided to cut it and found it interesting to add it to the composition.

![]({{site.baseurl}}/Failure-impro.JPG)

![]({{site.baseurl}}/Failure-outcome.JPG)


### Final outcome
Final outcomes look pretty good. After completing the objects, we created different compositions as a way to investigate light, form and material textures in relation to one another.

![]({{site.baseurl}}/Julia-positioning.JPG)

![]({{site.baseurl}}/Compo-orange-all-length.JPG)

![]({{site.baseurl}}/Veronica-positioning.JPG)

![]({{site.baseurl}}/Waffle-pyramid-doubles.JPG)

![]({{site.baseurl}}/Waffle-compo.JPG)

![]({{site.baseurl}}/Compo-trio.JPG)

![]({{site.baseurl}}/Combo-orange-all.JPG)

## Files

[Rhino composition]({{site.baseurl}}/resources/moulding-and-casting.3dm)

[Rhino sphere]({{site.baseurl}}/resources/W9Sphere.3dm)

[Rhino torus]({{site.baseurl}}/resources/W9Torus.3dm)

--------------

While we 3D printed and waited for the moulds to set in the initial steps, we designed the composition on Cinema 4D. This is the result we got:
![]({{site.baseurl}}/.jpg)
*Animation cinema 4D of composition*

![]({{site.baseurl}}/.jpg)
*Image cinema 4D of composition*

--------------

### Other ideas
I first imagined to recycle the copper tape I peeled of my board by melting it and then combine it to another metal to make a ring with a tiny part covered with copper. However I have asked around about it and the copper tape used for the PCB boards is not pure enough for a good quality result. It is better to try to find a good pieces of metal to melt and work with. Now if I want to stick to the idea of working with metals I need to find the right moulding material for it.

If I can not work with metals I will be making a stamp that will be used to make my "business" card. I would like to stamp any surfaces using ink to generate my professional information whenever needed but in a professional way rather than hand written or costly when printed. I am not sure if it will look good or would work but might want to use this opportunity to try.
