---
title: 8 | Embedded Programming
period: 13 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

## Install Drivers on Windows

As shown in the [Fab Academy tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html) for electronic production, I first had to download and install WinAVR.

## Programming the board
### ISP board

![]({{site.baseurl}}/ISP-programmer.jpeg)

I first had to program my ISP board from [week 4](https://mdef.gitlab.io/julia.danae/fabacademy/week4/) to then program the ATTINY44 board made in [week 6](https://mdef.gitlab.io/julia.danae/fabacademy/week6/).

Beforehand I had checked the circuit was properly working using the digital multi-meter and everything seemed to be working.

I downloaded the fab ISP zip file and extracted the file inside it. Once I got the folder, I used the flat cable to connect with my board. I used Gabor's mac computer to try set it up and he's given me some help on how to troubleshoot as he's done his. Following several attempts, the error message are still appearing even though the light appears green on the blue ISP programmer (as seen in the picture).

Here is a copy of the error messages given by the terminal after following each step of this guide [here](http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html#mac) The command *make fuse* gave an error message and so for the next steps.

{% highlight ruby %}
make: *** [fuse] Error 1
Gabor-5:fabISP_mac.0.8.2_firmware Gabor$ make clean
rm -f main.hex main.lst main.obj main.cof main.list main.map main.eep.hex main.elf *.o usbdrv/*.o main.s usbdrv/oddebug.s usbdrv/usbdrv.s
Gabor-5:fabISP_mac.0.8.2_firmware Gabor$ make hex
avr-gcc -Wall -Os -DF_CPU=20000000 -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c usbdrv/usbdrv.c -o usbdrv/usbdrv.o
avr-gcc -Wall -Os -DF_CPU=20000000 -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -x assembler-with-cpp -c usbdrv/usbdrvasm.S -ousbdrv/usbdrvasm.o
avr-gcc -Wall -Os -DF_CPU=20000000 -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c usbdrv/oddebug.c -o usbdrv/oddebug.o
avr-gcc -Wall -Os -DF_CPU=20000000 -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c main.c -o main.o
main.c:88:13: warning: always_inline function might not be inlinable [-Wattributes]
static void delay ( void )
^
avr-gcc -Wall -Os -DF_CPU=20000000 -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -o main.elf usbdrv/usbdrv.o usbdrv/usbdrvasm.ousbdrv/oddebug.o main.o
rm -f main.hex main.eep.hex
avr-objcopy -j .text -j .data -O ihex main.elf main.hex
avr-size main.hex
text data bss dec hex filename
0 2002 0 2002 7d2 main.hex
Gabor-5:fabISP_mac.0.8.2_firmware Gabor$ make fuse
avrdude -c avrisp2 -P usb -p attiny44 -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m

avrdude: stk500v2_command(): command failed
avrdude: stk500v2_program_enable(): bad AVRISPmkII connection status: Target not detected
avrdude: initialization failed, rc=-1
Double check connections and try again, or use -F to override
this check.

avrdude done. Thank you.

make: *** [fuse] Error 1
Gabor-5:fabISP_mac.0.8.2_firmware Gabor$
{% endhighlight %}

I will have to troubleshoot this board to continue the programming of other boards or use the AVRISP mkii from the Fab Lab for the input and output devices.


### Input board
More detailed information of the programming of my input board has been fully documented in [week 10](https://mdef.gitlab.io/julia.danae/fabacademy/week10/)'s documentation. I will only add a few important step I might not have included in week 10.

![]({{site.baseurl}}/TerminalPortSetupCheck.JPG)
These are the initial steps I had to do to make sure the board connects to my computer and that I can find it. For this I downloaded WinAVR and opened my computer's terminal and typed the instructions shown in the image. I found the board connection in device manager.

![]({{site.baseurl}}/ToolsArduinoIDE.JPG)
Then I installed the library and set configurations for the board.

Check for the final steps explanations in week 10.


### Files
[Touchpad traces .rml]({{site.baseurl}}/resources/TouchpadpsdTraces.rml)

[Input outline .rml]({{site.baseurl}}/resources/InputOutlineFINAL3.rml)

[Input traces .rml]({{site.baseurl}}/resources/InputTracesFINAL.rml)

[EAGLE schematic Input Device]({{site.baseurl}}/resources/TouchPad.sch)

After programming y input and output board, and had everything installed on my computer to be able to run the ISP board, I tried again to program it but was unable to have the ISP recognised on my computer. The Fab Lab programmer indicated a shortcut with the light blinking. I will have to check with the multimeter again every connection to detect where is the shortcut. But for now I have managed to program my other board and can use the Arduino for further use of those boards.

Touchpad code:

{% highlight ruby %}
#include

SoftwareSerial mySerial(1, 9);

void setup() {
  mySerial.begin(9600);
  pinMode(8,OUTPUT);
  pinMode(0,INPUT);
  pinMode(1,INPUT);
  pinMode(2,INPUT);
  pinMode(3,INPUT);
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);

}

void loop() {
  digitalWrite(8,LOW);
  int val0=analogRead(0);
  int val1=analogRead(1);
  int val2=analogRead(2);
  int val3=analogRead(3);
  int val4=analogRead(4);
  int val5=analogRead(5);
  int val6=analogRead(6);
  int val7=analogRead(7);
  mySerial.print(val0);
  mySerial.print("   ");
  mySerial.print(val1);
  mySerial.print("   ");
  mySerial.print(val2);
  mySerial.print("   ");
  mySerial.print(val3);
  mySerial.print("   ");
  mySerial.print(val4);
  mySerial.print("   ");
  mySerial.print(val5);
  mySerial.print("   ");
  mySerial.print(val6);
  mySerial.print("   ");
  mySerial.println(val7);
  delay(100);

}
{% endhighlight %}


### Links
[Atmel Studio](https://www.microchip.com/mplab/avr-support/avr-and-sam-downloads-archive)

[Tutorial Fab ISP set up](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html)

[Adafruit AVR programmer and SPI interface](https://learn.adafruit.com/usbtinyisp/drivers)

[Nikolas Kruchten Arduino repo](https://github.com/nicolaskruchten/arduino)

[Microcontroller datasheet](http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf)
