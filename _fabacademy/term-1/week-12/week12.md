---
title: 12 | Applications and implications
period: 20 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

## Scope of project
### Topics
This project is exploring the topics of E-waste, mass-consumption, material culture, 'Phygital'.
With the digital world revealing itself evermore within the physical, the layers of abstractions of the digital are multiplying, making it more difficult to understand the material culture that trying to follow the fluid nature of the digital culture. The constant flow of new data in the digital space is entirely redefining the space. However the physical tools supporting this space are the same with the only difference as the number of connections made between
components. Those complex hidden connections result in a disconnection between the interactions and their implications. Humans are no longer able to relate to the digital and physical consequences of the growing digital world. Depletion of abiotic resources is an economic and environmental problem that will depend on our interaction with future technologies.

### Question
How will humans engage with the physical manifestation of the digital interactions of the future?

### Intro to project
This project is an exploration of the materials responsible for the interactions with two separate world, yet dependent of one another, one as the abstract digital and the other as the tangible physical. The topics of electronic waste and material culture are the starting point for this exploration. It will carefully navigate between those two spaces to try understand the physical manifestation of this digital manipulation, and try to redefine the conversation between the two. The goal of this project is to offer insights on the current correlations and mediators between the two spaces, to engage and challenge the meaning of current interfaces and shape more responsible interactions. The power of material identity and its political implications have the potential to redefine the interface of the future now.

To what extent will interactions be physical or digital? What form will take the materials that mediate those conversations? What will redefine our interfaces? What will our interfaces be in the future? What will the physical manifestation evolve into? Will humans be able to engage the tangible in abstractions?

### Plan for project development

I have drawn the steps of the process of production and consumption of a digital device, as a way to find where exactly in this industry I wanted to intervene. This also allowed me to understand what would be the consequences of my intervention in the timeline of this instrument.

The outcome of this project should first prove my statement that changing the nature of our interaction with the tool that mediates between the digital and the physical, can alter the type and format of the product it generates in the process of use and end up redefining the kind of instrument/ digital device we ask for. Photography has rapidly evolved through time and is today a method of communication used by every digital consumers, to capture and share moments in specific time and place. Light waves have been increasingly privileged in photography, putting aside every other senses in the process of capturing a moment. This evolution has distanced the physical from the digital manipulations, resulting in a misunderstanding of the inter-dependence between those two spaces.

### What will it do?
I question what would a photograph of a moment would look like with touch as the input rather than light waves and what would the touch of another feel like if one could share it?
The initial idea is to create a form photography with touch as an alternative form of communication with another person that is not directly present. The first step is to choose what type of sensor I want to play with, in relation to touch, to understand technological feasibility. Then start creating the tool as a first prototype that allows this process of making and conversing.
For the final exhibition, ideally I would want people to be able to experience this interaction and create their own photographs to feel and share. For this I plan to use a touch sensor, my own Arduino board, about 10 servo motors, some plywood and possible some silicon.

### What has been done beforehand?
First a plan of our time was required by all student form the master in order to manage our precious time the best we can in accordance to the project. I have been building a narrative around my project by projecting it into the future but most importantly I have made a semiotic mapping of around the key actants and practices of my project which are communicating and touching seen as below.

 ![]({{site.baseurl}}/PracticeMap02.jpg)

### Materials required, processes used, systems to make
After spending some time to dismantle cameras and phones that had been thrown to waste, I have built a better understanding of the materials used to make a camera and phone functional.
As a starting point, I would like to use a specific touch sensor such as the capacitive sensor (touchpad or multitouch) to translate the data I get from touching certain areas of the pad into a physical output that could be using many servo motors for movement of pieces. Those pieces I will define them later when I will have defined more clearly what type of movement can be done and in function define those. The choice of material in this physical outcome is essential. It could be wood, or fabric but also could be a silicon, depending on the sensation I want to create along with the type of interaction. Once I start to play with those I might decide to scale it up and have a bigger surface or add touch sensor to the moving structure so that the feedback can be given in both ways, allowing a more fluid conversation with direct feedback form touch.

### What question need to be answered?
I am now asking a few questions that my prototype will need to answer. Could touch, 'the mother of the senses', be the new sense used to capture ephemeral information? Could we create a new method and format for photography, changing the way we communicate and use this a digital device capable of photography? How could one record, send, share touch photographs? How would one want to touch information?

### How will it be evaluated?
Any individual that will be trying this new from of communication will be able to evaluate its functionality and whether it carries the message I want it to carry. I would like to collect direct feedback from the individuals that will be testing the prototype by communicating in a small conversation or even through touch (if it works well) what are their thoughts and feedbacks.
