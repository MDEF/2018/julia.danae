---
title: 4 | Electronic production
period: 13 February 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

This week's assignment consisted in making a PCB board using a Roland MDX-20 Milling Machine. I had never used this machine before and watched the first explanations on how to use it, which helped me rapidly understand that the precision in the preparation is essential for it to produce a decent PCB board.

### Creating PCB file
![]({{site.baseurl}}/FabModulefilesetup.jpeg)

I started by selecting the hello.ISP.44 board I had to mill and downloaded both traces and outline from the Fab Academy week’s page. I then created the .rmv file for the Roland milling machine to read. I started with the traces using 1/64 end.-mill and then did the outline using 1/32 end-mill.

In Fab Modules website page, I imported the png file - double check the format - and selected the PCB board I want to make and which machine I am using to cut it. I then have to define the right set up for cutting by changing as follows:

**dpi**: minimum 500
What is usually already set up is 1270dpi and I have left it this way.

**zjog**: 10
The head will go all the way up between cuts.

**Offsets**: 4
The bigger the offset the more forgiving it is to make a mistake in the cutting and the easier it is to solder. I have set up 4 offsets, it is most used amount. If I want to have a clean board and take out all unnecessary copper left on the board, I can put the offsets to -1 (to fill) in the document pre-settings.

**Speed**: 4
I can put it slower for the inside milling of the board to be careful and precise but the outline of the board can use this speed securely.

**x**: 0
**y**: 0
**z**: 0

Z set up close to surface.

### Milling machine settings
![]({{site.baseurl}}/MSX20setup.jpeg)
![]({{site.baseurl}}/prepbrush.jpeg)
![]({{site.baseurl}}/Millingboardoutline.jpeg)
![]({{site.baseurl}}/brushnewboard.jpeg)

To start with, the surface on which my copper plate will be stuck needs to be cleaned and then I used double sided tape to fix it on the mdf board to avoid as much vibration as possible, which could lead to mess up the board milling.

Once placed, I set up my own x, y and z (height) to position the end-mill at the corner of my copper board. This avoids wasting material by cutting in the middle and is essential when using a copper plate that has already been used, and I had to make sure my board would fit in the space left.
To set up those axes, I have first put a 1/64 end-mill for the path cutting of my board. Once unscrewed, I precisely put my finger on the side of the point, not to let it fall and let it touch delicately the board. Then I can screw again and reset my z axis a little higher so that it won't carve in my board whenever it moves but only when it is shown in the file. Once the path done I create a second file for the outline only and change the end-mill with a 1/32.
When all settings are ready I can press the button to set up and delete all old files and select my file I have just created (one at a time) and then I can start cutting.
I have paused it a few seconds after the start and press the view mode so that I can make sure the cutting is happening correctly, meaning that it doesn't go too deep or not deep enough.

What happened... A first problem I encountered when making my first board was that the tape under didn't have the same height everywhere as it was folded in a corner and when I finished milling the board I noticed one side didn't mill through the whole layer of copper. I then decided to reset the Z a tiny bit lower so that it could mill again and this time it went through the layer. This problem was solved pretty simply however the next problem I encountered was that I milled the parts I needed instead of the outside because I inverted the black and white of the png file. I had to invert the colour of my file and start milling a new board as soon as I noticed.


### Soldering components
![]({{site.baseurl}}/boardcomponents.jpeg)
![]({{site.baseurl}}/image00017pcb.jpeg)

I then gathered all the components to solder on the PCB board. For this I have followed the online document and picture from the Fab Academy assignment page. I have had the opportunity to solder before, but the components were smaller and therefore slightly harder to solder on the board. I was advised to practice on old board first by unsoldering pieces and re-soldering them on another board. I then soldered those 15 components on my board.

![]({{site.baseurl}}/image00016pcb.jpeg)
My board finished.
