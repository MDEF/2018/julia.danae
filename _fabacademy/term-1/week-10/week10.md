---
title: 10 | Input devices
period: 20 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

For this week's input devices I will be recreating one of Neil's boards and will mill it and solder it and then program it to be able to read and use the sensor I will have chosen. Many sensors were suggested, I chose a capacitive touchpad as part of the step response sensors. To start with I decided to follow the work of [Matt Blackshaw](http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html).

### Sensor 1
The sensor I choose has been also made by [Josep Marti](http://fabacademy.org/2019/labs/barcelona/students/josep-marti/Week10.html) as part of his input week assignment, so I will follow his documentation in addition to the other. The capacitive touchpad measures input voltage on two grids of each sixteen copper pads layered on top of each other, using step response.
I had to look up what is a step response in order to understand what would I get from touching the pads. A step response is the time behaviour of the outputs of a general system when its input change form zero to one in a short amount of time. It will allow me to see how the system responds to a sudden input, in other words sense different values, and from there decide how I can use this first output to define the new output.

I cut the copper sheet for the touchpad.
![]({{site.baseurl}}/VynilCutTouchpad.jpeg)
![]({{site.baseurl}}/VynilCutSetup.jpeg)

### Circuit design
For the circuit design I started  by using Matt's design and tried to rearrange the routes directly in Eagle to try and solve the two connections that need many jumpers. I couldn't do it with the time I have so I recreated the design board of Josep which has been well done (without jump wires). I tweaked a little bit his design, changing a few routes to make them shorter and then exported the trace. I then went on Photoshop to do the outline and export the outline and the traces again.
![]({{site.baseurl}}/JosepInputSchematic.JPG)
![]({{site.baseurl}}/InputTraceComponents.JPG)
![]({{site.baseurl}}/TouchpadJoseppsd.JPG)

### Mill the board
![]({{site.baseurl}}/InputTracesFINAL.png)
![]({{site.baseurl}}/InputOutlineFINAL.png)

I mill the traces using 1/64" bit and the outline using 1/32" bit.
I then used [Fab Modules](http://fabmodules.org/) to create the .rml files and remembered how to prepare the files. I have used the pngs I have shown above and milled the board using th Roaland SRM-20 which is the one I have always used previously.
Issues I ran into this time: Basic mistake, I adjust dpi in setiings but did not check the size of the file which was about 500px times 500px. So once I milled the board, I got a 1cm board.

![]({{site.baseurl}}/MiniBoardMistake.jpg)

![]({{site.baseurl}}/InputTracesFabModulesFINAL.JPG)
![]({{site.baseurl}}/InputOutlineFabModulesFINAL.JPG)

### Solder the board
![]({{site.baseurl}}/InputComponents.jpg)
![]({{site.baseurl}}/InputTraceComponents.JPG)

I then collected the components from the lab, wrote them down on my sketch book and updated the [google sheet](https://docs.google.com/spreadsheets/d/1kYkuEQHJd3KMiPEm66VuyQaV-U6ornS0trmqu0Via-8/edit#gid=1779571110).

The components I soldered are as follow:
- (1) ATTINY 84 (more memory than the 44)
- (1) AVRISPSMD
- (1) Resistor 10K (1002)
- (8) Resistor 1M (1004)
- (2) Resistor 0,0
- (1) Capacitor ceramic 10UF
- (1) Resistor 499 ohlm
- (1) LED green clear

Soldering is something I enjoy to do and find quite relaxing too. After soldering I tried to double check that the connections were well made and that everything was correctly  soldered to try save some time in the next step.

![]({{site.baseurl}}/FinalinputPicture.jpeg)

### Touch pad  
I then had to stick the copper tape onto a piece of plastic to vinyl cut it. First I wanted to change the shape of the pads and di so using Illustrator. I then saved the document as a .png but had to resave it as a .jpeg to be able to use it in the vinyl cutter program.

![]({{site.baseurl}}/TouchpadGridIllustrator.JPG)
![]({{site.baseurl}}/VynilCutSetup.jpg)

I had to first make sure to put the two wheels that take in my material to be at the same level as the white lines (but not the same) to then place my material. I then asked the machine to measure my material and from there resized the drawing onto the sheet. I have cute them quite small but for now it is not an issue.

![]({{site.baseurl}}/VynilCutTouchpad.jpg)

**Next I will have to connect the two pads**


### Programming the board
Once I have finished to solder the board, I had to program it. Before, I checked with a multi-meter that all my connections were made correctly. I tested the critical areas like the six pin and microcontroller. Everything biped when touching ground and ground and same for vcc and vcc, which means all is good.

I then had a first attempt in programming the board within the Arduino IDE by following the instruction in the [documentation](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html), but could not understand what was needed for the boards I had made. I asked for help and got kindly helped by the instructor.

I started by connecting the board with the FTDI cable. To know what is the correct way, I looked at my schematic of the board to know in the six pins which one is which and be able to connect MISO, MOSI, SCK, RESET, VCC, and GND of the programmer to the corresponding pins on the ATtiny - and plugged the programmer AVRISP mkii to my computer.

To program the ATtiny44 microcontroller on this input device, I first had to download the AVRISP mkii driver for my computer to be able to recognise it and work with it to program my board. For this we had some problems as the ones we found online and downloaded did not the job. We later found ATMEL studio. However it didn't seem to work as my computer could still not recognise the device. We then tried to install AVR programmer and SPI interface from Adafruit, which I think was the one that worked for the input device. Once plugged, I opened Arduino IDE and checked the Board, processor, clock and programmer I am using to then do the 'burn bootloader'. My input device is now programmed.

![]({{site.baseurl}}/ATtiny4484Datasheet.png)

To then test my board I had to look at the corresponding pins of the microcontroller - ATtiny 84 - I am using to know which pins to write in the code0. I did this by looking at the microcontroller datasheet as shown above.

**Touchpad connected and code**
I then connected the first copper layer to the first four pins by looking at which pins they connect to on the microcontroller and same for the second board, creating a touch pad.


### Sensor 2
After discussing my project with the instructors, I have decided to make my second input (and output) as a light sensor to help me out save time in the testing of my project. This sensor I will combine to the servo motor output PCB board. The simple proximity of my finger to the light sensor could first show with the turning off or on of the light that it is functioning and when connected to the servo motor output it can allow me to quickly activate the movement and test its functionality. How I have combined the two will be explained in the output devices documentation.

### Files
[Touchpad traces .rml]({{site.baseurl}}/resources/TouchpadpsdTraces.rml)

[Input outline .rml]({{site.baseurl}}/resources/InputOutlineFINAL3.rml)

[Input traces .rml]({{site.baseurl}}/resources/InputTracesFINAL.rml)

[EAGLE schematic Input Device]({{site.baseurl}}/resources/TouchPad.sch)

{% highlight ruby %}
#include

SoftwareSerial mySerial(1, 9);

void setup() {
  mySerial.begin(9600);
  pinMode(8,OUTPUT);
  pinMode(0,INPUT);
  pinMode(1,INPUT);
  pinMode(2,INPUT);
  pinMode(3,INPUT);
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);

}

void loop() {
  digitalWrite(8,LOW);
  int val0=analogRead(0);
  int val1=analogRead(1);
  int val2=analogRead(2);
  int val3=analogRead(3);
  int val4=analogRead(4);
  int val5=analogRead(5);
  int val6=analogRead(6);
  int val7=analogRead(7);
  mySerial.print(val0);
  mySerial.print("   ");
  mySerial.print(val1);
  mySerial.print("   ");
  mySerial.print(val2);
  mySerial.print("   ");
  mySerial.print(val3);
  mySerial.print("   ");
  mySerial.print(val4);
  mySerial.print("   ");
  mySerial.print(val5);
  mySerial.print("   ");
  mySerial.print(val6);
  mySerial.print("   ");
  mySerial.println(val7);
  delay(100);

}
{% endhighlight %}

-----------------

### Class on input

Why do we need sensors?
Visualise changes happening in our environment to sense our environment (but not only).

Sensors were designed because they were giving outputs which can contribute to closing loop systems. However not every technology has a closed loop system. Example of the 3D printers (...)
Sensors have moved from being scientific to things we carry at all time on us, such as our mobile devices (GPS, compass, ...) Today sensors can, not only to see, but achieve something to move, to change, to control...
Sensors in smartphones are using multiples sensors for single function all together to give as much accuracy as possible.

Three things to take into consideration when designing using sensors:
- Sensors are not only used to measure environments (such as pollution issues) but can be equally used within machines to make it functional.
- Data fusion as a way to increase accuracy of results and functionality.
- Might remove unnecessary sensors when you have enough data for possible changes to be predictable.

How to choose / wire / code for a sensor?
Sensors could be selected as components (+affordable, -coding) but also as devices (+accessibility, -price).
Sensors interfaces (...)
Capacitive sensing (...)

Learning how to make a sensor with basic knowledge and elements form off shelves at home, can contribute to building a resilience to changes.

[Adafruit](https://www.adafruit.com/) is an online platform providing basic knowledge to create at home designs (for instance sensors) using the appropriate components.

Breakout board (...)

Select an input for this week assignment
Light
Detect light from the environment, the sun position change...
http://academy.cba.mit.edu/classes/input_devices/index.html

### Links

[Atmel Studio](https://www.microchip.com/mplab/avr-support/avr-and-sam-downloads-archive)

[Tutorial Fab ISP set up](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html)

[Adafruit AVR programmer and SPI interface](https://learn.adafruit.com/usbtinyisp/drivers)

[Nikolas Kruchten Arduino repo](https://github.com/nicolaskruchten/arduino)
