---
title: 2 | Computer aided design
period: 30 January 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

## Testing programs
As I have a background in design, I have already tried a few software for 2D and 3D designs such as Sketchup, ArchiCAD, AutoCAD, and Rhino. The one I have stuck with and feel most comfortable working with is Rhino. I will therefore use Rhino for further testing and experiment with Rhino Grasshopper. Another of the programs I wanted to start experimenting with is Cinema4D. I don't know much about it but I am very interest in filming/animation and would be a much better software to render designs with the possibility to explore different materiality of those designs.

### Rhinoceros
I started designing a ring to then mould it with metal for the moulding and casting week.
First I drew simple surface which I have cut out the shapes to extrude. Then I used command curve to make the external surface and Sweep1 to form the curved surface. One of the two is not working properly but will try to solve this for the moulding and casting assignment.

![]({{site.baseurl}}/RingRhino.JPG)

### Rhino Grasshopper
I decided to test some more complex grasshopper because I have learned some basics for computer-controlled machining assignment. I have composed a script as a way to form a from fractals as a way to explain the idea of growth and have defined some variables for each of the number sliders as a way to play with the outcome of playing with such variables. How could those define the physical outcome.

![]({{site.baseurl}}/ParametricDesignGrasshopper.JPG)
![]({{site.baseurl}}/ParametricDesign2.JPG)

### Cinema 4D
I started a design from scratch on this program to understand how the basics work but I could also import a design I have done in Rhino and render it and play with different material properties in C4D. I am trying to only play with the state of matter of each object I create in this scenario. It takes some time to play with the behaviour of those objects as there is a lot of variables to play with.

![]({{site.baseurl}}/MographDesignTest.JPG)

## Design files

[Rhino design]({{site.baseurl}}/resources/RingMould.3dm)

[Rhino Grasshopper design](Parametric-Design-Project-Fractalsgh)

[C4D design](Mograph2surfaces.c4d)

----------

## Intro to the concepts of computer-aided design
### 3D design
Difference between body and components
**Body** is a shape.
**Components**

### Parametric design
I have experimented a little with parametric design in the beginning. I wanted to try create a logic in the  formation of a shape using fractals.

### Rhino and Grasshopper
Rhino as a sketch based model however not parametric.
Grasshopper plug-in allows parametric design.

### Fusion 360
- Computer-aided software, manufacturing and engineering.
- Generative design.
- Projects saved on cloud, easier access from other computers.
- Timeline / history of design (allows you to go back and transform)

*Sketch palette* on the right with all the tools.

Start with *sketch*
Press *d* is dimension
Change parameters to play with parametric aspect. Can change name and dimension an dthen change directly in dimension tool writing the name. Quick change to the wanted dimensions.

### Monolith
A Voxel-based modelling engine.
Good to represent material for cheap 3D printing
Good for experimentation

## Extra info
### Vectors
are the mathematical description needed to create an image or visual, meaning it translates relative equation into an image.
The advantage of using vector is the infinite quality no matter how much you zoom in, and its ease to edit.

### Raster
are the smallest unit of bitmap images, meaning one pixel is translated into one colour.
large amount of information contained in any raster image.

### Vectoral programmes
are showing raster images, why?
It is showing the final result of the file.
SVG (Scalable Vector Graphic)

### Resolution
today on most recent screens you get 300 to 600 pixel per inch (ppi) which makes the interaction much more smooth.
Fun fact: a human with perfect sight can see difference in quality up to 876ppi. However the average human eye distinguishes a pixel density of 320ppi at 10cm away distance.

300ppi is the average for presentation.
72ppi for online use
If I plan to store a file for a long time, 180ppi is enough.

### Colour work space
Computers can only represent 30% of the real colours. Have to be aware that every human sees colour in different ways, and same as computer screens. The original luminosity of the screen can alter the perceptions of the colours.
Online is better to use SRGB so that everyone cans see the same colours.
*For prints, the difference between CMYK and RGB ...*

### Image format
How is the image saved:
- jpg - most flexible format (but loses more easily quality)
- png - lossless format with excellent compression
- tiff - camera use it to take picture without losing quality
- psd - photoshop version of tiff
- raw-neff - non processed image your camera can do. For RAW put it in your raster editing program and transform the format to be usable.
RAW image is about ten times better quality, ten times bigger than a jpeg.

### Photoshop configuration
Set up 70% use of RAM memory to allow your computer to use that space instantly during the time you are working on Photoshop.
