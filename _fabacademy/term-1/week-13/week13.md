---
title: 13 | Wildcard week
period: 24 May 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

## Wood bending
For the wildcard week, I have decided to join the workshop in making wood bending. I have had the chance to see master of wood bending to make chairs in Stockholm and had already been interested in trying wood bending but using the vacuum forming machine.

![]({{site.baseurl}}/prep-longboard-examples.jpg)

### CNC foam
First the a thick piece of blue dense foam has been cut out in the CNC in the shape and more importantly respecting the curves of the  longboard. I haven't been able to follow the details of this first step but was able to see the result.

### Laser cutting
Then the wood to make the board was selected for its properties of being flexible and have some form of resistance in flexion. Three pieces were cut for the three layers of the board, with the middle using a thinner wood and cut against the grain of the wood, whilst the others have been cut in length along the grain. This allows a resistance when layered on top of each other. I haven't been able to assist neither to the laser cutting but was able to see the result when we started making the board.
![]({{site.baseurl}}/longboard-wood-layers.jpg)

We later decided to laser cut a piece of fabric (fibre composite) to glue as the last top layer of the board as a finish. For this we set a power of 80 and a low frequency of 500 to not burn the fabric in the cutting.
![]({{site.baseurl}}/longboard-fabric.jpg)

### Layers materials
We started preparing the table by cutting and layering different sheets. The first layer is a green plastic sheet to protect the foam mould from soaking Epoxy.
![]({{site.baseurl}}/sheets-layers-types.jpg)
![]({{site.baseurl}}/prep-sheets-layers.jpg)

Next we placed the three layers of wood, a lyer of fabric for the finish style and added a blue layer of thin plastic sheet. This blue sheet has small holes which allows to trap the surplus of Epoxy later in the process. The next layer could be a sponge like sheet (like a foam) but we used the fabric to absorb a little and then placed on top a green plastic sheet as the final layer.
![]({{site.baseurl}}/prep-layers-longboard.jpg)
![]({{site.baseurl}}/longboard-prep-first-layer.jpg)
![]({{site.baseurl}}/longboard-layers-done.jpg)

### Epoxy
After having prepared all the layers and cut the materials, I mixed the Epoxy 2 part A for 1 part B. The type is shown in the image below.
![]({{site.baseurl}}/Epoxy-partAB.jpg)

Epoxy resin is a thermosetting polymer that hardens when mixed with a catalyst (hardener) agent. I had a look at the health and safety information on this product and wrote down below the most important instructions.

- Wear gloves
- Avoid splashing product onto bare skin or into eyes
- Avoid skin contact via contamination of clothing
- Avoid inhalation of vapours or dust

We then covered each wood board with the mix and added the layer of fabric to which we also added a layer of Epoxy. Then added the blue, fabric and green sheets.
![]({{site.baseurl}}/longboard-epoxy-wood.jpg)
![]({{site.baseurl}}/longboard-epoxy-last-layer.jpg)
![]({{site.baseurl}}/longboard-epoxy-fabric.jpg)

### Vacuum forming
The last step was actually quite simple. We took the block of layers with the mould and placed in the vacuum forming. Before closing we made sure to have every layer aligned to each other and then closed. We switched to start, the vacuum started taking all the air out of the surface. We left it in there for 24hours.
![]({{site.baseurl}}/vacuum-forming-longboard.jpg)
![]({{site.baseurl}}/longboard-vacuum-forming.jpg)

It now looks like an amazing deck! :)

### Sanding
The next step would be to sand a little the sides and the wood under and then mount some trucks on the deck and add wheels to make it a functionable longboard, ready to test.
