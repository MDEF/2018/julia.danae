---
title: 6 | Electronics design
period: 27 February 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---
![]({{site.baseurl}}/image00005.jpeg)

### Software for circuit board design
![]({{site.baseurl}}/eagle vcc set up.jpeg)

To redraw the [Hello Echo](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) board I have used the free software EagleCAD. I first created a new file to create a new schematic and then downloaded from the Fab Library a file named *Fab*, in which all the components necessary and available in the workspace. I then got to Library manager and browse the same *Fab* file.

I checked the components necessary to reproduce the Hello Echo Board:
- AVRISPSMD 6-pin Programming Header: Used to program the board.
- Attiny44A Microcontroller: Difference of potential energy between two points.
- FTDI-SMD-Header: Powers the board and allows board to talk to computer.
- Resonator - 20MHz (CRYSTAL): Difference of potential energy between two points.
- Resistors - value 10k ohm: Purpose: pull-up resistor.
- Resistor - value 499 ohms: Purpose: current limiting resistor for LED.
- Capacitor - 1uF
- Button - 6mm Switch
- LED (Light Emitting Diode): LEDs have polarity - the side with the line is the cathode and connects to the ground side.
- Ground
- VCC

I add, place and label with VCC, MOSI the component ATTINY44 - SSU. I then added Ground and VCC and AVRISPMD.
I right click SCK to breakout the following pins: MISO, RST, and SCK and then label format for those.
Note that USCK and SCK both mean the same, it is a clock.
After placing the components, I switched from the board to the schematic to start putting order between them to untangle the links. Once I have ordered them each by hand, I start creating the routes with a grid setting of 5mm, to have enough distance, and a route thickness of 6. I have later been told it was better if the routes were thicker, so I retraced the route with 16. Now back to the board, I add crystal. By right clicking when placing a new component, I can rotate it for a better link. I then connected the PBI (crystal+) and PBO. I then add CAP-UNPOLARIZED C1206 Fab. I add value for +C1 of 22pF and repeat for +C2.
Now I added a LED 206 Fab package and then choose between two options, to plug it in 12 or in 13. As shown in the picture, it can be either a source current or a sink current, but as I understood, both are good to use. Last component I add is the 6mm_switch6mm_switch.

![]({{site.baseurl}}/eagle vcc set up3.jpeg)

![]({{site.baseurl}}/eagle vcc set up4orderroute.jpeg)

I later discovered I could use RATSNEST to order the connections automatically but it doesn't do the job quite correctly and it is no more fun.
Before the export, I make sure to select only the top and dimension layers. I export the image with 1000dpi and monochrome. Once saved, I go to Tools, select DRC (design rules), load and then check for errors. I only had two errors, I didn't connect properly the routes.

I have realised some components were missing such as the button the resistors... I have tried to add the missing components to the board however the *fab* file is missing when I look to add an item but is present in the library manager. I decided to delete the file and download it again from our Fab Academy web page. I then reinstalled and have now again the file working. I think the file had been moved or missing some content.

![]({{site.baseurl}}/Final4ResistorsPCB.jpeg)

I had to add the missing parts along with four jumpers as I couldn't complete the circuit without it. I added three unpolarized capacitors (22pF) and four resistors (0,0) as jumpers. I have finally got the circuit board ready to mill or cut.  

![]({{site.baseurl}}/4RPCB.png)

### Milling the circuit board
![]({{site.baseurl}}/DesignCircuitBoardweek7.jpeg)

Now I have to make the PNG a SVG, using [eagle2svg-1.1.ulp](http://eagle.autodesk.com/eagle/ulp?page=22), and clean up the lines a little before I can open the file with Fab Modules. To do this I have exported as image and selected the option monochrome and the put the resolution up to 1000dpi.
To create the outline I have imported my file in illustrator and traced an outline of 20px thickness.
I have decided to use a vinyl cutter to cut the copper tape off the plastic layer. I created a png file that I exported as a jpeg to run on the software CutStudio. Once the piece of copper and plastic inserted in the machine, I will see the workable area calculated by the cutter. Here I has 117 x 100 mm. I then go to file > cutting > properties > > import > jpeg. To see the properties I open Fabmodules and check the dimension of my board. I can then rescale the imported file to the correct dimension which should fit in the working area. I then go to image outline > extract contour line. Finally I adjust the power of the machine to cut to 140 gf and start the cutting.
Because soldering is a little more complicated on this surface than a milled board, I have cut four examples of the board in case I failed one when removing the copper or soldering the components.

![]({{site.baseurl}}/image00002.jpeg)

![]({{site.baseurl}}/image00003.jpeg)

![]({{site.baseurl}}/image00004.jpeg)

### Soldering the circuit board

![]({{site.baseurl}}/image00007.jpeg)

![]({{site.baseurl}}/image00008.jpeg)


### Files

[ATTINY44 schematic.brd]({{site.baseurl}}/resources/ATTINY44 schematic.brd)

[ATTINY44 schematic.sch]({{site.baseurl}}/resources/ATTINY44 schematic.sch)


### Workflows demonstrations used in circuit board design
- [Serial communication](https://learn.sparkfun.com/tutorials/serial-communication)
- [highlowtech.org](http://highlowtech.org/?p=1695)
- [Electronic circuit simulator](http://www.falstad.com/circuit/)

**Asynchronous**
Constantly trying to grasp all information at every time.

**Synchronous**
Add concept of time with a cock signal, this can allow to control the information flow at a regular rhythm. We've done this with a SPI before. It has 6 pins
